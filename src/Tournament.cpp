#include "include/Tournament.h"
#include "include/Utility.h"
#include "include/constants.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

using namespace std;

extern vector<string> gBreadcrumbs;
extern void printBreadcrumbs();
extern const unsigned int c_maxNameLen;
extern const string c_fileSuffix;
namespace sd {

/**
 * Attempts to read a tournaments data from file.
 * 
 * format:
 * <m_name>
 * <m_active>
 * <m_played>
 * <holeCount>
 * <holeName>
 * <holePar>
 * ... repeat <holeCount> times
 * <playerCount>
 * <playerName>
 * <stroke> <stroke> ... // stroke per hole
 * ... repeat <playerCount> times
 * end of file
 * 
 * @param in - stream object to read data from
 */
void Tournament::readFromFile(ifstream& in) {
    if(in) {
        string holeName;
        string playerName;
        int holeCount = 0;
        int playerCount = 0;
        int playerStroke = 0;
        int holePar = 0;


        getline(in, m_name);
        in >> m_active;
        in.ignore();
        in >> m_played;
        in.ignore();
        in >> holeCount;
        in.ignore();
        for (int i = 0; i < holeCount; i++) {
            getline(in, holeName);
            in >> holePar;
            in.ignore();
            m_holes.push_back(Hole(holeName, holePar));
        }
        in >> playerCount;
        in.ignore();
        for (int i = 0; i < playerCount; i++) {
            getline(in, playerName);
            m_players.push_back(Player(holeCount, playerName));
            for(int j = 0; j < holeCount; j++) {
                in >> playerStroke;
                in.ignore();
                if(playerStroke != 0) {
                    m_players.back().setStroke(playerStroke, j+1);
                }
            }
        }
    } else {
        cout << "Couldn't locate file.\n";
    }
}


/**
 * Writes a tournaments data to a file with its own name.
 *
 */
void Tournament::writeToFile(const string directory) {
    ofstream out(directory + "/" + m_name + c_fileSuffix);
    if(out) {
        string holeName;
        string playerName;
        int holeCount = 0;
        int playerCount = 0;
        int playerStroke = 0;
        int holePar = 0;

        out << m_name << "\n";
        out << m_active << "\n";
        out << m_played << "\n";
        out << m_holes.size() << "\n";
        for (auto h : m_holes) {
            out << h.getName() << "\n";
            out << h.getPar() << "\n";
        }
        out << m_players.size() << "\n";
        for (auto p : m_players) {
            out << p.getName() << "\n";
            for(int j = 0; j < m_holes.size(); j++) {
                out << p.getStroke(j+1) << ((j < m_holes.size() - 1)? " ": "\n");
            }
        }
    } else {
        cout << "Could not create/open file";
    }
}


/**
 * Sets the name of a tournament. Tournament name doubles as file name
 * if saved to file otherwise other name is specified.
 *
 * @param name - new name
 * @param maxLen - max allowed name length
 */
void Tournament::setName(string name, const int &maxLen) {
    if(name.length() <= maxLen) {
        m_name = name;
    } else {
        cout << "Error: Name too long.\n";
    }
}


/**
* Menu interface for editing active tournament.
*
*/
void Tournament::menu()   {
    gBreadcrumbs.push_back("manage tournament");
    char input = 0;
    if(!m_active && !m_played && isPlayable()) { 
        sd::clearConsole();
        printBreadcrumbs();
        cout<< "\n  Your tournament is currently not active.\n"
            << "  To be able to start registering strokes, the tournament has to be active.\n"
            << "  Once a tournament is set as active you can no longer edit it.\n\n"
            << "  Set your current tournament as active? Y/N: ";
        input = sd::readChar();
        if(input == 'Y') {
            m_active = true;
            cout << "Tournament is now active. Press Enter to proceed...";
            getchar();
        }
    }
    if (m_active || m_played) {
        do {
            sd::clearConsole();
            printBreadcrumbs();
            cout<< "\n  Active Tournament: " << m_name << "\n";
            if (!m_played) { 
                cout  << "\tU - Update Stroke \n"; 
            }
            cout<< "\tD - Display Score \n"
                << "\tR - Display Participants and Holes\n"
                << "\tS - Save tournament\n"
                << "\tF - Finalize and end tournament \n"
                << "\tB - Back to " << gBreadcrumbs[gBreadcrumbs.size()-2]<< "\n";
            input = sd::readChar();
            switch(input) {
                case 'U': if(!m_played){updateStroke();}       break;
                case 'D': displayScore();       break;
                case 'R': printRegistries();    break;
                case 'F': finalize();           break;
                case 'S': {
                    writeToFile(c_saveDir);  
                    cout << "File saved. Press Enter...";
                    getchar();
                } break;
                default:                        break;
            }
            sd::clearConsole();
        } while (input !='B');
    }
    gBreadcrumbs.pop_back();
}


/**
* Menu for updating stroke per hole.
*
*/
void Tournament::updateStroke()  {
    gBreadcrumbs.push_back("update stroke");
    char input = 0;
    do {
        sd::clearConsole();
        printBreadcrumbs();
        cout << "\nUpdate Stroke: \n"
             << "\tC - Current Hole \n"
             << "\tT - Select a Hole (not in MVP) \n"
             << "\tB - Back to " << gBreadcrumbs[gBreadcrumbs.size()-2]<< "\n";
        input = sd::readChar();
        switch (input) {
            case 'C':  strokeForCurrentHole();    break;
            case 'T': /*selectHole()*/;  break;
            default: ;                  break;
        }
        sd::clearConsole();
    } while (input !='B');
    gBreadcrumbs.pop_back();
}


/**
* Menu for displaying score.
*
*/
void Tournament::displayScore() {
    gBreadcrumbs.push_back("show results");
    char input = 0;
    do {
        sd::clearConsole();
        printBreadcrumbs();
        cout << "\nShow Results: \n"
             //<< "\tT: Score per team (not in MVP) \n"
             << "\tP: Score per player \n"
             << "\tB - Back to " << gBreadcrumbs[gBreadcrumbs.size()-2]<< "\n";
        input = sd::readChar();
        switch (input) {
            case 'P': printScoreByPlayer();      break;
            default:                        break;
        }
        sd::clearConsole();
    } while (input !='B');
    gBreadcrumbs.pop_back();
}


/**
 * Prints the score as stroke under par of all players for each Hole in the
 * Tournament's m_holes vector.
 *
 */
void Tournament::printScoreByPlayer() {
    gBreadcrumbs.push_back("score by player");
    sd::clearConsole();
    printBreadcrumbs();
    cout <<  setw(c_maxNameLen + 2) << left <<"Score by player:" << "Hole:\n";
    cout <<  setw(c_maxNameLen + 2) << left << "";
    for (int i = 0; i < m_holes.size(); i++) {
        cout << setw(4) << i+1;
    }
    cout << "Tot\n\n";
    for (auto &p : m_players) {
        int score = 0; int scoreTot = 0;
        cout<< setw(c_maxNameLen + 2) << left << p.getName();
        for ( int i = 0; i < m_holes.size(); i++) {
            if(p.getStroke(i+1)) {
                score = m_ruleset.scoreConvert(p.getStroke(i+1), m_holes[i].getPar());
                cout << setw(4) << score;
                scoreTot += score;
            } else {
                cout << setw(4) << 'x';
            }
        }
        cout << " " << scoreTot << "\n";
    }
    cout << "\nPress Enter to return...";
    getchar();
    gBreadcrumbs.pop_back();
}


/**
 * Gives the user the ability to set a Tournament as finished, meaning it can
 * no longer have its strokes updated. m_played will be set true in the event
 * of a Tournament being finalized.
 *
 */
void Tournament::finalize() {
    bool gameDone = true;
    char input;
    for (int i = 0; i < m_holes.size(); i++) {
        for (int j = 0; j < m_players.size(); j++) {
            if(m_players[j].getStroke(i+1) == 0) {
                gameDone = false;
                break;
            }
        }
    }
    if(gameDone) {
        cout << "Once the game has been finalized it can no longer be updated.\n"
            << "\nFinalize tournament? Y/N:";
        input = sd::readChar();
        if(input == 'Y') {
            cout << "Tournament has been finalized. Press Enter...";
            m_played = true;
        } else {
            cout << "Tournament not finalized. Press Enter...";
            getchar();
        }
    } else {
        cout << "The tournament lacks strokes for some or all players.\n"
             << "Once the game has been finalized it can no longer be edited.\n"
             << "\nFinalize tournament? Y/N:";
        input = sd::readChar();
        if(input == 'Y') {
            cout << "Tournament has been finalized. Press Enter...";
            m_played = true;
        } else {
            cout << "Tournament not finalized. Press Enter...";
            getchar();
        }
    }
}


/**
 * Registering score for players in order for the current hole.
 * Current hole is the first hole where stroke has yet to be registered for all
 * players.
 * 
 */
void Tournament:: strokeForCurrentHole() {
    gBreadcrumbs.push_back("stroke current hole");
    string input = "1";
    bool done = true;
    int holeNr = m_holes.size(); //Shows last hole as default

    do {
        sd::clearConsole();
        printBreadcrumbs();
        for (int i = 0; i < m_holes.size(); i++){
            for (int j = 0; j < m_players.size(); j++){
                if (!m_players[j].getStroke(i + 1)) {
                    holeNr = i + 1;
                    done = false;
                    goto found;
                }
            }
        }
        found:
        cout << "Updating stroke for hole nr. " << holeNr << " "
             << m_holes[holeNr-1].getName()<< ": 0 to exit.\n\n";
        cout << setw(c_maxNameLen + 2)<< left << "Name" << "Stroke\n";
        for (int i = 0; i < m_players.size(); i++) {
            if (m_players[i].getStroke(holeNr)) {
                cout << setw(c_maxNameLen + 2) << left << m_players[i].getName()
                    << "  " << m_players[i].getStroke(holeNr) << '\n';
            } else {
                cout << setw(c_maxNameLen + 2) << left << m_players[i].getName();
                cout << "(1-10): ";
                input = sd::readString();
                if (sd::isNumber(input) && 1 <= stoi(input) && stoi(input) <= 10) {
                    m_players[i].setStroke(stoi(input), holeNr);
                    done = true;
                } else if(sd::isNumber(input) && stoi(input) == 0) {
                    cout << "Exiting... Press Enter.\n";
                    done = true;
                    break;
                } else {
                    cout << "Not a valid stroke value";
                    done = false; //Ensures the do-while continues when break;
                    break; //Breaks for-loop and starts over from top
                }
            }
        }
        if (done && stoi(input) != 0) {
            cout << "\nStroke registered for all players.";
        }
        getchar();
    } while (!done);
    gBreadcrumbs.pop_back();
}


/**
 * Returns the name of the Tournament object.
 * 
 * @return string - Name of Tournament.
 */
string Tournament::getName() const {
    return m_name;
}


//HANDLING PLAYERS

/**
 * Adds a player to the Tournament object and updates the track count of the 
 * player to match the current size of the m_holes vector.
 *
 * @param player - Player object added to m_players vector
 */
void Tournament::addPlayer(sd::Player player){
    player.updateTrackCount(m_holes.size());
    m_players.push_back(player);
}


/**
 * Gives the user the ability to edit or delete an existing player.
 * 
 */
void Tournament::editPlayer(int playerNr){
    gBreadcrumbs.push_back("edit player");
    char input = 0;
    do {
        sd::clearConsole();
        printBreadcrumbs();
        cout<< "Editing player nr. " << playerNr << ":\n"
            << "\tN - Edit Name:" << m_players[playerNr-1].getName() << "\n"
            << "\tD - Delete Player\n"
            << "\tB - back to " << gBreadcrumbs[gBreadcrumbs.size()-2] << "\n";
        input = sd::readChar();
        switch(input) {
            case 'N': {
                string name;
                cout << "Name: ";
                name = sd::readString();
                if(!name.empty()) {
                    m_players[playerNr-1].setName(name, c_maxNameLen);
                }
            } break;
            case 'D': {
                cout << "You're about to delete player nr. " << playerNr
                    << " Confirm? Y/N: ";
                input = sd::readChar();
                if (input == 'Y') {
                    cout << "Player has been deleted. Press enter..." << "\n";
                    m_players.erase(m_players.begin() + playerNr -1);
                    input = 'B';
                } else {
                    cout << "Player not deleted. Press enter..." << endl;
                    getchar();
                }
            }

        }
    } while (input != 'B');
    gBreadcrumbs.pop_back();
}


/**
 * Gives the user the ability to edit or delete an existing hole.
 * 
 */
void Tournament::editHole(int holeNr){
    gBreadcrumbs.push_back("edit hole");
    char input = 0;
    do {
        sd::clearConsole();
        printBreadcrumbs();
        cout<< "Editing hole nr. " << holeNr << ":\n"
            << "\tN - Edit Name:" << m_holes[holeNr-1].getName() << "\n"
            << "\tD - Delete Hole\n"
            << "\tB - back to " << gBreadcrumbs[gBreadcrumbs.size()-2] << "\n";
        input = sd::readChar();
        switch(input) {
            case 'N': {
                string name;
                cout << "Name: ";
                name = sd::readString();
                if(!name.empty()) {
                    m_holes[holeNr-1].setName(name, c_maxNameLen);
                }
            } break;
            case 'D': {
                cout << "You're about to delete hole nr. " << holeNr
                    << " Confirm? Y/N: ";
                input = sd::readChar();
                if (input == 'Y') {
                    cout << "Hole has been deleted. Press enter..." << "\n";
                    m_holes.erase(m_holes.begin() + holeNr -1);
                    input = 'B';
                } else {
                    cout << "Hole not deleted. Press enter..." << endl;
                    getchar();
                }
            }

        }
    } while (input != 'B');
    gBreadcrumbs.pop_back();
}


/**
 * Returns the amount of players in a tournament.
 *
 * @return - Playercount
 */
int Tournament::getPlayerCount() const {
    return m_players.size();
}

/**
 * Returns the amount of holes in a tournament.
 * @return - hole count
 * 
 */
int Tournament::getHoleCount() const {
    return m_holes.size();
}


/**
 * Adds a hole object to m_holes vector.
 * 
 * WARNING: Do not use on an active or played tournament, will delete data.
 * @see updateTrackCount()
 * @param hole - hole to be added.
 */
void Tournament::addHole(sd::Hole hole) {
    m_holes.push_back(hole);
    for(sd::Player &p : m_players) {
        p.updateTrackCount(m_holes.size());
    }
}


/**
 * Checks wether a gold hole has already been addeed to the tournament.
 *
 * @param holeName - Name of tournament to be checked.
 * @return true if holeName is found among m_holes.
 */
bool Tournament::hasHole(std::string holeName) {
    auto p = [=](Hole h){return h.getName()  == holeName; };
    return any_of(m_holes.begin(), m_holes.end(), p);
}


/**
 * Checks if a tournament is in a playable state with at least two players
 * and at least one hole to play.
 *
 * @return true
 * @return false
 */
bool Tournament::isPlayable() {
    return (m_players.size() > 1 && m_holes.size() && m_name != "");
}


/**
 * Checks if a tournament is in the process of being played.
 * 
 */
bool Tournament::isActive() {
    return m_active;
}


/**
* Prints out all players in active tournament

*/
void Tournament::printPlayers(){
    for (int i = 0; i < m_players.size(); i++){
            cout << '\t' << i+1 << ". " << m_players[i].getName() << '\n';
    }
}


/**
* Prints out all holes in active tournament
*
*/
void Tournament::printHoles(){
    for (int i = 0; i < m_holes.size(); i++){
        cout << '\t' << i+1 << ". " << m_holes[i].getName() << '\n';
    }
}


/**
 * Prints a tournaments registered holes and players as a menu item.
 * 
 */
void Tournament::printRegistries() {
    sd::clearConsole();
    gBreadcrumbs.push_back("registries");
    printBreadcrumbs();
    cout << "\n  Registered Holes:\n";
    printHoles();
    cout << "\n  Registered Players:\n";
    printPlayers();
    cout << "\n  Press enter to return...";
    getchar();
    gBreadcrumbs.pop_back();
}


/**
 * Checks if a tournament has already been played and finalized. 
 * This logic is used to determine if a tournament can be edited or not.
 * 
 * @return true 
 * @return false 
 */
bool Tournament::isPlayed(){
    return m_played;
}

} //End of namespace sd

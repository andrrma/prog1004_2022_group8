/*************************************************************************//**
 * @file Hole.cpp
 * @author Paul Bjørneng, Jørgen Teigen
 * @brief Implementation of Ruleset.h
 * @version 0.1
 * @date 2022-03-22
 ****************************************************************************/
#include "include/Ruleset.h"

namespace sd {


/**
 * Calculates the score as stroke under parameters
 * 
 * @param stroke - Stroke of Player for current Hole.
 * @param par - par of current Hole.
 * @return score - stroke under par.
 */
int Ruleset::scoreConvert(const int &stroke, const int &par) {
    int score = stroke - par;
    return score;
}


/**
 * 
 * 
 */
int Ruleset::handicap(const int &choice) {
    if (choice != 0)
        return choice;
    
    return -1;
}

} //End of namespace
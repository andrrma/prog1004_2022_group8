/*************************************************************************//**
 * @file Player.cpp
 * @author André Marthinsen
 * @brief Implementation of Player.h
 * @version 0.1
 * @date 2022-03-10
 ****************************************************************************/

#include <iostream>
#include <string>
#include <cstring>
#include <cctype>
#include "include/Player.h"
#include "include/Utility.h"

using std::string;

const int MAXNAMELEN = 50;

namespace sd {


/**
 * Constructs a new Player:: Player object
 * 
 */
Player::Player() {
    //m_handicap = 0;
    //m_playerNr = 0;
    //m_teamNr   = 0;
};


/**
 * Construct a new Player:: Player object
 * 
 * @param tracks - Number of tracks for a tournament
 */
Player::Player( const int &tracks, const string name) {
    for(int i = 0; i < abs(tracks); i++) {
        m_score.push_back(0);
    }
    m_name = name;
}


/**
 * Sets player-name. Alphabetical and whitespace only. Max 50 characters.
 * 
 * @param newName - New player name.
 * @param maxLen  - Max name length, up to 50 characters.
 */
void Player::setName(const std::string &newName, const int &maxLen) {
    std::string name = trimWhitespace(newName);
    if(name.length() <= maxLen) {
        if(!hasSymbol(name, true)) {
            m_name = name;
        } else {
            std::cout << "Error: Name contains illegal characters\n";
        }
    } else {
        std::cout << "Error: Name too long.\n";
    }
}


/**
 * Name getter.
 * 
 * @return - string : Name of player. 
 */
string Player::getName() const {
    return m_name;
}


/**
 * Sets a players score for a particular track.
 * 
 * @param score - The players new score.
 * @param trackNr - Number of track score is updated for.
 */

void Player::setStroke( const int &stroke, const int &trackNr ) {
    if ( 1 <= trackNr && trackNr <= m_score.size()) {
        m_score[trackNr-1] = stroke;
    } else {
        std::cout << "Error setScore: Track nr. out of bounds.\n";
    }
}


/**
 * Returns player score for a particular track. 0 = no registered score.
 * -2 = Error.
 * 
 * @param trackNr 
 * @return int - Score.
 */
int Player::getStroke( const int &trackNr ) const {
    if ( 1 <= trackNr && trackNr <= m_score.size()) {
        return m_score[trackNr-1];
    } else {
        std::cout << "Error getScore: Track nr. out of bounds.\n";
        return 0;
    }
}


/**
 * Updates the number of tracks.
 * WARNING: Resets all saved scores.
 * 
 * @param trackNr - Total tracks in tournament.
 */
void Player::updateTrackCount(const int &trackNr) {
    if ( 0 <= trackNr) {
        m_score.clear();
        for(int i = 0; i < trackNr; i++) {
            m_score.push_back(0);
        }
    } else {
        std::cout << "Error updateTrackCount: Track count cannot be negative.\n";
    }
}


//*****************************************************************************
// Currently unused member functions.
// Intended for use with other game modes that might see implementation.
//*****************************************************************************


/**
 * Sets the players number to any positive integer.
 * 
 * @param playerNr - int > 0
 */
/*
void Player::setPlayerNr( const int &playerNr ) {
    if (playerNr > 0) {
        m_playerNr = playerNr;
    } else {
        std::cout << "Error setPlayerNr: Negative player number not allowed.\n";
    }
}
*/

/**
 * Returns the players number
 * 
 * @return int - player-number
 */
/*
int Player::getPlayerNr() const {
    return m_playerNr;
}
*/

/**
 * Sets a players team number.
 * 
 * @param teamNr - New team number.
 */
/*
void Player::setTeamNr( const int &teamNr ) {
    if (teamNr > 0) {
        m_teamNr = teamNr;
    } else {
        std::cout << "Error setTeamNr: Negative team number not allowed.\n";
    }
}
*/


/**
 * Returns the players team number.
 * 
 * @return int - Team number of player.
 */
/*
int Player::getTeamNr() const {
    return m_teamNr;
}
*/

/**
 * Sets a players handicap from 0 to 54.
 * 
 * @param handicap - Players new handicap.
 */
/*
void Player::setHandicap( const int &handicap ) {
    if ( 0 <= handicap && handicap <= 54) {
        m_handicap = handicap;
    } else {
        std::cout << "Error setHandicap: Illegal handicap value.\n";
    }
}
*/

/**
 * Handicap getter.
 * 
 * @return int - player handicap
 */
/*
int Player::getHandicap () const {
    return m_handicap;
}
*/



}
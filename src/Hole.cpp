/*************************************************************************//**
 * @file Hole.cpp
 * @author Paul Bjørneng, Jørgen Teigen
 * @brief Implementation of Hole.h
 * @version 0.1
 * @date 2022-03-22
 ****************************************************************************/

#include <iostream>
#include <string>
#include "include/Hole.h"
#include "include/Utility.h"
using namespace std;


namespace sd {


/**
 * Hole default constructor.
 * 
 */
Hole::Hole() {
    m_name    = "";
    m_par     = 0;
    //m_trackNr = 0;
}


/**
 * Construct a new Hole:: Hole object
 * 
 * @param name - name of hole object
 * @param par - par of hole object
 */
Hole::Hole(std::string name, int par) {
    setName(name, 25);
    setPar(par);
}


/**
 * Sets hole name. Alphabetical only.
 * 
 */
void Hole::setName(const string &newName, const int &maxLen) {
    if(newName.length() <= maxLen) {
        if(!hasSymbol(newName, true)) {
            m_name = newName;
        } else {
            cout << "Error: Name contains illegal characters\n";
        }
    } else {
        cout << "Error: Name too long.\n";
    }

}


/**
 * Gets name of Hole object. 
 * 
 * @return - string : Name of hole
 */
string Hole::getName()const {
    return m_name;
}


/**
 * Sets par. Min 0 and max 10.
 * 
 */
void Hole::setPar(const int &banePar) {
    if(banePar > 0 && banePar <= 10){
        m_par = banePar;

    } else {
        cout << "Error: Par cant be larger then 10 or smaller then 1";
    }

}


/**
 * Gets par of Hole object.
 * 
 * @return - int : the par
 */
int Hole::getPar()const {
    return m_par;
}


/**
 * Sets track number. Positive integer only.
 * 
 */
/*
void Hole::setTrackNr(const int &trackNr) {
    if(trackNr > 0){
        m_trackNr = trackNr;

    } else {
      cout << "Error: Negative track number not allowed.\n";
    }
}
*/

/**
 * Gets the track number.
 * 
 * @return - int : the track number
 */
/*
int Hole::getTrackNr()const {
    return m_trackNr;
}
*/

} //End of namespace

/**
 * @file main.cpp
 * @author your name (you@domain.com)
 * @brief A program for administration of a mini-golf tournament.
 * @version 0.1
 * @date 2022-03-23
 *
 * @copyright Copyright (c) 2022
 */

#include "include/Player.h"
#include "include/Utility.h"
#include "include/Team.h"
#include "include/Tournament.h"
#include "include/Hole.h"
#include "include/constants.h"

#include <Windows.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <fstream>
#include <filesystem> //NB! Needs mingw64 v8.2+ and C++17 standard to compile

using namespace std;
namespace sf = std::filesystem;


extern const unsigned int c_maxNameLen; ///< max length of all names
extern const string c_saveDir;          ///< default save/load directory
extern const string c_fileSuffix;       ///< filename suffix
extern const string c_holesFile;        ///< filename + suffix of holes savefile


void printBreadcrumbs();
void savePrompt();
void manageHolesMenu();
void newHoleMenu();
void deleteHole();
void loadHolesFromFile();
void writeHolesToFile();
void displayHoles();
void newOrEditTournamentMenu( bool edit );
void loadTournamentMenu();
void newPlayerTournamentMenu(sd::Tournament &tournament);
void addHoleTournamentMenu(sd::Tournament &tournament);


HANDLE gConsole = GetStdHandle(STD_OUTPUT_HANDLE); ///< handle for setting color
sd::Tournament* gTournament = nullptr;  ///< current Tournament in memory.
vector<sd::Hole> gHoles;                ///< Holes in memory
vector<string> gBreadcrumbs;            ///< Strings indicating where a user is

//******************************************************************************
// Main Program
//******************************************************************************

int main() {

    char input = 0;
    
    loadHolesFromFile();

    gBreadcrumbs.push_back("main menu");

    do {
        sd::clearConsole();
        printBreadcrumbs();
        cout << "\n  Menu:\n"
             << "\tN - New Tournament\n"
             << ((gTournament != nullptr && !gTournament->isPlayed() && !gTournament->isActive()) ? "\tE - Edit Tournament\n":"")
             << "\tO - Open Tournament\n"
             << ((gTournament != nullptr && gTournament->isActive())?"\tA - Manage Active Tournament\n":"")
             << ((gTournament != nullptr && gTournament->isPlayable())?"\tA - Activate Tournament\n":"")
             << ((gTournament != nullptr && gTournament->isPlayed())?"\tA - View Old Tournament\n":"")
             << "\tM - Manage Holes\n"
             << "\tQ - Quit\n";
        input = sd::readChar();
        switch(input) {
        case 'N': newOrEditTournamentMenu(false);                       break;
        case 'E': { 
            if(gTournament != nullptr && !gTournament->isPlayed() && !gTournament->isActive()) {
                newOrEditTournamentMenu(true); 
            }
        } break;
        case 'O': loadTournamentMenu();                                 break;
        case 'A': if(gTournament != nullptr) { gTournament->menu(); }   break;
        case 'M': manageHolesMenu();                                    break;
        case 'Q': if(gTournament != nullptr) { savePrompt(); }          break;
        default :                                                       break;
        }
    } while (input != 'Q');

    writeHolesToFile();

}

//******************************************************************************
// Global function definitions
//******************************************************************************


/**
 * Prints where in the program a user is to console in gray text.
 * 
 */
void printBreadcrumbs() {
    SetConsoleTextAttribute( gConsole, 8);
    auto p = [](unsigned char c){ if(c == ' '){c = '_';} return c; };
    for (int i = 0; i < gBreadcrumbs.size(); i++) {
        string temp = gBreadcrumbs[i];
        transform(temp.begin(), temp.end(), temp.begin(), p);
        cout << "/" << temp;
    }
    cout << "\n";
    SetConsoleTextAttribute( gConsole, 15);
}


/**
 * Prints Holes stored in gHoles vector to console with name and par.
 * 
 */
void displayHoles() {
    cout<< "\n  All Holes:\n\n"
        << setw(31) << left << "\tName" << "Par\n";
    for(int i = 0; i < gHoles.size(); i++) {
        cout<< "\t" << setw(2) << right << i+1 << ". " << setw(c_maxNameLen + 2)
            << left <<  gHoles[i].getName() << gHoles[i].getPar() << "\n";
    }
}

//******************************************************************************
// HOLES: Functions related to management of the gHoles vector.
//******************************************************************************


/**
 * Loads holes to gHoles vector from 'holes_save_data.dta' in savedata folder
 * 
 */
void loadHolesFromFile() {
    ifstream in(c_saveDir + '/' + c_holesFile);
    if(in) {
        string name;
        int par;
        getline(in, name);
        while(!in.eof()) {
            in >> par;
            in.ignore();
            gHoles.push_back(sd::Hole(name, par));
            getline(in, name);
        }
    } else {
        cout << "No savefile for golf holes found.\n";
        getchar();
    }
}


/**
 * Writes all files in gHoles to file with filename constant c_holesFile
 * specified in constants.h
 * 
 */
void writeHolesToFile() {
    ofstream out(c_saveDir + '/' + c_holesFile);
    if (out) {
        for (int i = 0; i < gHoles.size(); i++) {
            out << gHoles[i].getName() << "\n" << gHoles[i].getPar();
            if(i < gHoles.size()-1) {
                out << "\n";
            }
        }
    } else {
        cout << "Unable to write holes to savefile.\n";
        getchar();
    }
}


/**
 * Main menu for management of holes stored in gHoles.
 * 
 * @see newHole()
 * @see deleteHole()
 */
void manageHolesMenu() {
    gBreadcrumbs.push_back("manage holes");
    char input = 0;
    do {
        sd::clearConsole();
        printBreadcrumbs();
        cout << "\n  Menu: Manage Holes\n"
             << "\tS - Show Holes\n"
             << "\tN - New Hole\n"
             << "\tD - Delete Hole\n"
             << "\tB - Back to " << gBreadcrumbs[gBreadcrumbs.size()-2]<< "\n";
        input = sd::readChar();
        switch(input) {
        case 'S': {
            sd::clearConsole();
            gBreadcrumbs.push_back("show holes");
            printBreadcrumbs();
            displayHoles();
            cout << "\n\n Press Enter to return:";
            getchar();
            gBreadcrumbs.pop_back();
        } break;
        case 'N': newHoleMenu();    break;
        case 'D': deleteHole(); break;
        case 'B':  ; break;
        default :  ; break;
        }
    } while (input != 'B');
    gBreadcrumbs.pop_back();
}


/**
 * Menu for creating a new hole and storing it to the global gHoles vector.
 * Can chop this one up into smaller segments if it gets too cluttered.
 * 
 */
void newHoleMenu() {
    gBreadcrumbs.push_back("new hole");
    sd::Hole newHole;
    char input = 0;
    bool nameSet = false;
    bool parSet = false;

    do {
        sd::clearConsole();
        printBreadcrumbs();
        cout << "\nMenu: New Hole\n"
             << "\tN - Name: " << newHole.getName() << "\n"
             << "\tP - Par: " //Prints nothing if par = 0
             << ((newHole.getPar())? to_string(newHole.getPar()) : "") << "\n"
             << "\tF - Finalize and save\n"
             << "\tB - Back to " << gBreadcrumbs[gBreadcrumbs.size()-2]<< "\n";
        input = sd::readChar();

        switch(input) {
        case 'N': { //Setting and controlling valid name for a hole
            string holeName;
            cout << "Hole name: ";
            holeName = sd::readString();
            newHole.setName(holeName, c_maxNameLen);
            if(newHole.getName() != holeName) {
                cout << "No name set\n"
                     << "Enter to continue\n";
                     getchar();
            } else {
                nameSet = true;
            }
        } break;
        case 'P': { // Setting and controlling valid parameters for a hole
            string newPar;
            cout << "Par (1 - 10): ";
            newPar = sd::readString();
            if (sd::isNumber(newPar)) {
                newHole.setPar(stoi(newPar));
                if(newHole.getPar() && newHole.getPar() == stoi(newPar)) {
                    parSet = true;
                } else {
                    //class provides error message
                    getchar();
                }
            } else {
                cout << "Syntax error: No par set.\n";
                getchar();
            }
        } break;
        case 'F': { // Tries to store new hole. Warning given if data not complete.
            if(!(nameSet && parSet)) {
                cout << "Hole not complete. Cannot store data.\n";
                getchar();
            } else {  // Asks for confirmation
                cout<< "\nSave hole named " << newHole.getName() << " with par "
                    << newHole.getPar() << " as hole nr " << gHoles.size()+1 << "? Y/N\n";
                input = sd::readChar();
                if (input == 'Y') { // Yes, adds new hole, returns to prev menu
                    gHoles.push_back(newHole);
                    writeHolesToFile();
                    input = 'B';
                    cout << "Hole saved. Press enter...";
                } else { // No, resumes menu operation.
                    input = '0';
                }
                getchar();
            }
        } break;
        case 'B': { //Tries to cancel. Gives a warning that nothing has been saved.
            if(parSet || nameSet) {
                cout << "Hole not stored. Still cancel? Y/N: ";
                input = sd::readChar();
                if(input == 'Y') {
                    input = 'B';
                } else {
                    input = '0';
                }
            }
        } break;
        default : break;
        } //End of switch
    } while (input != 'B');
    gBreadcrumbs.pop_back();
}


/**
 * Menu for deleting holes stored in gHoles.
 * 
 */
void deleteHole() {
    gBreadcrumbs.push_back("delete");
    int holeNr = 0;
    char chInput = 0;
    string input;
    do {
        sd::clearConsole();
        printBreadcrumbs();
        displayHoles();
        cout << "\nInput number of the hole you wish to delete, 0 to exit: ";
        input = sd::readString();
        if ( sd::isNumber(input) ) {
            holeNr = stoi(input);
            if (holeNr == 0) {
                cout << "Exiting. Press Enter to return...";
                chInput = 'Q';
            } else if( 1 <= holeNr && holeNr <= gHoles.size()) {
                cout<< "About to erase hole nr." << holeNr << " "
                    << gHoles[holeNr-1].getName() << ". Confirm? Y/N: ";
                chInput = sd::readChar();
                if(chInput == 'Y') {
                    gHoles.erase(gHoles.begin() + holeNr-1);
                    writeHolesToFile();
                    cout << "Hole has been erased. Press enter...\n";
                } else {
                    cout << "Hole not erased. Press enter...\n";
                }
            } else {
                cout << "Error: Integer out of range";
            }
        } else {
            cout << "Error: Not an integer.";
        }
        getchar();
    } while (chInput != 'Q');
    gBreadcrumbs.pop_back();
}

//******************************************************************************
// TOURNAMENTS: Functions related to the management of tournaments.
//******************************************************************************


/**
 * Gives the user the option to save the current tournament if gTournament
 * isn't a nullptr.
 * 
 */
void savePrompt() {
    char input;
    if(gTournament != nullptr) {
        cout << "Would you like to save the current tournament first? Y/N: ";
        input = sd::readChar();
        if ( input != 'N') {
            gTournament->writeToFile(c_saveDir);
            cout << "File " << gTournament->getName() << c_fileSuffix << " saved.\n";
            getchar();
        }
    }
}


/**
* Menu for making new tournament or editing an existing one.
*
* @param edit - true if editing existing tournament
*/
void newOrEditTournamentMenu( bool edit) {
    string name = (edit)? "edit tournament" : "new tournament";
    gBreadcrumbs.push_back(name);
    sd::Tournament temp;
    if(edit) {
        temp = *gTournament;
    }
    char input = 0;
    int playerNr = 0;
    int holeNr = 0;
    string strInput;
    bool saved = false;
    if (!edit) {
        savePrompt();
    }
    do {
        sd::clearConsole();
        printBreadcrumbs();
        //Prints menu names depending on context, edit or new
        cout << ((edit)? "\n  Edit Tournament:  \n"  : "\n  New Tournament:  \n")
             << ((temp.getName().empty())? "\tN - Tournament Name: " : "\tN - Edit Name: ")
             << temp.getName() << "\n"
             << "\tP - Add Player \n"
             << "\tH - Add Holes \n"
             << "\tE - Edit Player/Hole\n"
             << "\tD - Display Holes and Players\n"
             << "\tS - Save " << ((edit)? "changes\n":"\n")
             << "\tB - Back to " << gBreadcrumbs[gBreadcrumbs.size()-2]<< "\n";
        input = sd::readChar();
        switch (input) {
            case 'N': {
                string tournamentName;
                cout << "Tournament Name: ";
                tournamentName = sd::readString();
                temp.setName(tournamentName, c_maxNameLen);
                if(temp.getName() == ""){
                    cout << "\nNo name set! \n"
                         << "Enter to continue...";
                } 
            } break;
            case 'P': newPlayerTournamentMenu(temp);          break;
            case 'H': addHoleTournamentMenu(temp);            break;
            case 'E': {
                do {
                    cout <<"Player or Hole? P/H, C to cancel: ";
                    input = sd::readChar();
                    switch(input){
                    case 'P': {
                        sd::clearConsole();
                        printBreadcrumbs();
                        cout << "\n  Registered Players:\n";
                        temp.printPlayers();
                        cout << "\nEnter the nr. of a player you'd like to edit or 0 to exit: ";
                        strInput = sd::readString();
                        if ( sd::isNumber(strInput) ) {
                            playerNr = stoi(strInput);
                            if (playerNr == 0) {
                                cout << "Exiting. Press Enter to return...";
                                getchar();
                                input = 'C';
                            } else if( 1 <= playerNr && playerNr <= temp.getPlayerCount()) {
                                temp.editPlayer(playerNr);
                                input = 'C';
                            } else {
                                cout << "Error: Integer out of range";
                                getchar();
                            }
                        } else {
                            cout << "Error: Not an integer.";
                            getchar();
                        }
                    } break;
                    case 'H': {
                        sd::clearConsole();
                        printBreadcrumbs();
                        cout << "\n  Registered Holes:\n";
                        temp.printHoles();
                        cout << "\nEnter the nr. of a hole you'd like to edit or 0 to exit: ";
                        strInput = sd::readString();
                        if ( sd::isNumber(strInput) ) {
                            holeNr = stoi(strInput);
                            if (holeNr == 0) {
                                cout << "Exiting. Press Enter to return...";
                                getchar();
                                input = 'C';
                            } else if( 1 <= holeNr && holeNr <= temp.getHoleCount()) {
                                temp.editHole(holeNr);
                                input = 'C';
                            } else {
                                cout << "Error: Integer out of range";
                                getchar();
                            }
                        } else {
                            cout << "Error: Not an integer.";
                            getchar();
                        }
                    }  break;
                    default : break;
                    } //End of switch
                } while (input != 'C');


            }  break;
            case 'D': {
                temp.printRegistries();
            } break;
            case 'S': {
                char input;
                if (temp.isPlayable()) {
                    if(gTournament == nullptr) { 
                        gTournament = new sd::Tournament;
                    }
                    if(edit && gTournament->getName() != temp.getName()) {
                            remove((c_saveDir + "/" +  gTournament->getName() + c_fileSuffix).c_str());
                    }
                    *gTournament = temp;
                    gTournament->writeToFile(c_saveDir);
                    saved = true;
                    cout << "Tournament has been saved and can now be played.";
                } else {
                    cout << "The tournament is currently unplayable. Save regardless? Y/N: ";
                    input = sd::readChar();
                    if (input == 'Y') {
                        cout << "Tournament saved. Press enter...";
                        if(gTournament == nullptr) { 
                            gTournament = new sd::Tournament;
                        }
                        if(edit && gTournament->getName() != temp.getName()) {
                            remove((c_saveDir + "/" +  gTournament->getName() + c_fileSuffix).c_str());
                        }
                        *gTournament = temp;
                        gTournament->writeToFile(c_saveDir);
                        saved = true;
                    } else {
                        cout << ((edit)? "Changes":"Tournament") << " not saved. Press enter...";
                    }
                }
                getchar();
            } break;
            case 'B': {
                if (!saved) {
                    cout << ((edit)? "Changes":"Tournament") << " not saved. Return anyway? Y/N: ";
                    input = sd::readChar();
                    if (input == 'Y') {
                        input = 'B'; //Breaks do-while and returns
                    }
                }
            }
            default: break;
        } //End of switch
    } while (input != 'B');
    gBreadcrumbs.pop_back();
}


/**
 * Menu giving the user the option of loading data from a tournament savefile.
 * 
 */
void loadTournamentMenu() {
    gBreadcrumbs.push_back("open tournament");
    map<int, string> filenames;
    string input;
    savePrompt();
    do {
        sd::clearConsole();
        printBreadcrumbs();
        sf::path path{sf::current_path()}; //Creates a path to program directory,
        path.append(c_saveDir);            // then appends save directory. 
        if(sf::exists(path)) {
            int fileNr = 1;              
            for( auto it : sf::directory_iterator{path}) {
                string temp = it.path().filename().string();
                if(temp.find(c_fileSuffix) != -1) {
                    filenames.insert(pair<int, string>(fileNr,  temp));
                    fileNr++;
                }
            }
            cout << "  Files:\n";
            for(auto f : filenames) {
                cout << "\t" << f.first << ". " << f.second << "\n";
            }
            cout << "\n\nEnter number of file you would like to open, or 0 to cancel: ";
            input = sd::readString();
            if(sd::isNumber(input)) {
                int fileNr = stoi(input);
                if(filenames.count(fileNr)) {
                    cout << "Open " << filenames[fileNr] << "? Y/N: ";
                    input = sd::readChar();
                    if(input[0] == 'Y') {
                        if(gTournament != nullptr) {delete gTournament; }
                        gTournament = new sd::Tournament;
                        ifstream in(c_saveDir +"/" + filenames[fileNr]);
                        gTournament->readFromFile(in);
                        input = "0";
                    } else {

                    }
                }
            }
        } else {
            cout << "Savefile directory not found.\n";
        }
    } while (input != "0");
    gBreadcrumbs.pop_back();
}


/**
* Menu for creating new player and adding it to a tournament
*
*/
void newPlayerTournamentMenu( sd::Tournament &tournament) {
    gBreadcrumbs.push_back("add player");
    sd::Player newPlayer;
    char input = 0;
    do {
        sd::clearConsole();
        printBreadcrumbs();
        cout<< "\n  Add Player: \n"
            << ((newPlayer.getName().empty())? "\tN - Name: ": "\tN - Edit Name: ")
            <<  newPlayer.getName() << "\n"
            << "\tS - Save \n"
            << "\tB - Back to " << gBreadcrumbs[gBreadcrumbs.size()-2]<< "\n";
        input = sd::readChar();
        switch (input) {
            case 'N': {
                string playerName;
                cout << "Player Name: ";
                playerName = sd::readString();
                newPlayer.setName(playerName, c_maxNameLen);
                if(newPlayer.getName() == ""){
                   cout << "\nNo name set! \n"
                        << "Enter to continue...";
                }
            } break;

            case 'S': {
                if( newPlayer.getName() != "") {
                    tournament.addPlayer(newPlayer);
                    cout<< "Added player " << newPlayer.getName()
                        << " as player nr. " << tournament.getPlayerCount();
                    input = 'B';
                } else {
                    cout << "No name registered, cannot add player.\n";
                }
                getchar();
            } break;

            case 'B': {
                if(newPlayer.getName() != "") {
                    cout << "Player not saved. Still go back? Y/N:";
                    input = sd::readChar();
                    if(input == 'Y') {
                        input = 'B';
                    } else {
                        input = '+';
                    }
                }
            } break;
        }
    } while (input != 'B');
    gBreadcrumbs.pop_back();
}


/**
 * Interface for adding a hole to a tournament during setup.
 * Should add display of tracks that have been added.
 *
 */
void addHoleTournamentMenu(sd::Tournament &tournament) {
    gBreadcrumbs.push_back("add hole");
    int holeNr = 0;
    char chInput = 0;
    string input;
    vector<int> added;
    do {
        sd::clearConsole();
        printBreadcrumbs();
        displayHoles();
        cout << "\nEnter the nr. of a hole you'd like to add or 0 to exit: ";
        input = sd::readString();
        if ( sd::isNumber(input) ) {
            holeNr = stoi(input);
            if (holeNr == 0) {
                cout << "Exiting. Press Enter to return...";
                chInput = 'Q';
            } else if( 1 <= holeNr && holeNr <= gHoles.size()) {
                if(!tournament.hasHole(gHoles[holeNr-1].getName())) {
                    cout<< "About to add hole nr." << holeNr << " "
                        << gHoles[holeNr-1].getName() << ". Confirm? Y/N: ";
                    chInput = sd::readChar();
                    if(chInput == 'Y') {
                        tournament.addHole(gHoles[holeNr-1]);
                        added.push_back(holeNr);
                        cout << "Hole added. Press enter...\n";
                    } else {
                        cout << "Hole not added. Press enter...\n";
                    }
                } else {
                    cout << "Hole already added. Press enter...\n";
                }
            } else {
                cout << "Error: Integer out of range";
            }
        } else {
            cout << "Error: Not an integer.";
        }
        getchar();
    } while (chInput != 'Q');
    gBreadcrumbs.pop_back();
}

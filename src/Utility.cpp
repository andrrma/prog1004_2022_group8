#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <Windows.h>
#include <cctype>
#include <limits>
#include "include/Utility.h"

using namespace std;
extern HANDLE gConsole;

namespace sd {


/**
 * Trims down whitespace in a string. i.e "  3    asdd  " becomes "3 asdd".
 *
 * @param str - String trimmed of whitespace.
 * @return std::string - New string trimmed of excess whitespace.
 */
std::string trimWhitespace(std::string str) {
    if (str.size()) {
        while(str.at(0) == ' ') {      //Removes preceding whitespace
            str.erase(0, 1);
        }                              //Removes trailing whitespace
        while(str.size() > 0 && str.at(str.size()-1) == ' ') {
            str.erase(str.size()-1, 1);
        }
        int spaces = 0;
        for(auto it = str.begin(); it!=str.end();) { //Reduces multiple ' ' chars
            if(*it == ' ') {                         //to a single space by iterating
                if(spaces) {                         //through the string, deleting
                    str.erase(it);                   //any space following another
                } else {                             //space.
                    spaces++; it++;
                }
            } else {
                if(spaces) {spaces--; };
                it++;
            }
        }
        return str;
    } else {
        return std::string("");
    }
    
}


/**
 * Returns true if a string contains a symbol such as -*+/. etc.
 *
 * @param str - String to be checked for symbols.
 * @param ignoreSpace - Will not count whitespace as a symbol if true.
 * @return true - if string contains a non alphabetical symbol.
 */
bool hasSymbol(std::string str, bool ignoreSpace) {
    if(ignoreSpace) {
        auto p = [](char c){return (!isalpha(c) && c != ' '); };
        return (any_of(str.begin(), str.end(), p));
    } else {
        auto p = [](char c){return (!isalpha(c)); };
        return (any_of(str.begin(), str.end(), p));
    }
}


/**
 * Checks if a string contains a at leaste one digit 0-9.
 *
 * @param str - String to be checked for digits.
 * @return true - if string contains a digit 0-9.
 */
bool hasDigit(std::string str) {
    auto p = [](char c){return isdigit(c); };
    return (any_of(str.begin(), str.end(), p));
}


/**
 * Checks if a string can be interpreted as an integer, i.e "120".
 *
 * @param str - String to be checked.
 * @return true - if string can be interpreted as an integer.
 */
bool isNumber(std::string str) {
    if(str.size()) {
        auto p = [](char c){return isdigit(c) != 0; };
        return (all_of(str.begin(), str.end(), p));
    } else {
        return false;
    }
}


/**
 * Windows specific method for clearing console window. To be used when going
 * from one menu to another to keep the screen uncluttered.
 * 
 */
void clearConsole()
{
    COORD topLeft = { 0, 0 };
    CONSOLE_SCREEN_BUFFER_INFO screen;
    DWORD written;

    GetConsoleScreenBufferInfo(gConsole, &screen);
    FillConsoleOutputCharacterA(
        gConsole, ' ', screen.dwSize.X * screen.dwSize.Y, topLeft, &written
    );
    FillConsoleOutputAttribute(
        gConsole, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE,
        screen.dwSize.X * screen.dwSize.Y, topLeft, &written
    );
    SetConsoleCursorPosition(gConsole, topLeft);
}


/**
 * Reads a character from console and returns the char in uppercase or '+' for
 * as an error message.
 * 
 * @return char - Read character in uppercase, or '+' if not alpha
 */
char readChar() {
    char input = 0;
    std::cin >> input;
    std::cin.ignore(1000, '\n');
    if(isalpha(input)){
        return(toupper(input));
    } else {
        return '+';
    }
}


/**
 * Reads a string and returns it stripped of superfluous whitespace.
 * 
 * @return string - Input string trimmed of whitespace
 */
string readString() {
    string input;
    getline(cin, input);
    input = sd::trimWhitespace(input);
    return input;
}

} //End of namespace


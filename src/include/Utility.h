#ifndef UTILITY_H
#define UTILITY_H

#include <vector>
#include <string>

namespace sd {
    std::string trimWhitespace(std::string str);
    bool hasSymbol(std::string str, bool ignoreSpace);
    bool hasDigit(std::string str);
    bool isNumber(std::string str);
    void clearConsole();
    char readChar();
    std::string readString();

}

#endif
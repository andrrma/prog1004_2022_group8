#ifndef Ruleset_H
#define Ruleset_H




namespace sd{

/**
* Checking the diffrence between stroke and par and return the score.
* Checks and return if handicap.
*/


class Ruleset{

private:


public:
   int scoreConvert(const int &stroke, const int &par);
   int handicap(const int &choice);

};




}




#endif // Ruleset_H

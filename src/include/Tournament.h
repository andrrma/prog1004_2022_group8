#ifndef TOURNAMENT_H
#define TOURNAMENT_H

#include "Player.h"
#include "Team.h"
#include "Ruleset.h"
#include "Hole.h"

#include <string>
#include <vector>
#include <fstream>

namespace sd {

class Tournament {
private:
    std::string m_name; //doubles as file name
    sd::Ruleset m_ruleset;
    std::vector<sd::Hole> m_holes;
    //std::vector<sd::Team> m_teams;
    std::vector<sd::Player> m_players;
    bool m_active;
    bool m_played;

public:
    Tournament(){m_active = false; m_played = false;};
    void setName(std::string name, const int &maxLen);
    std::string getName() const;
    void readFromFile(std::ifstream& in);
    void writeToFile(const std::string directory);
    void menu();
    void updateStroke();
    void strokeForCurrentHole();
    void displayScore();
    void finalize();
    void printScoreByPlayer();
    void printPlayers();
    void printHoles();
    void printRegistries();
    void addPlayer(sd::Player player);
    void editPlayer(int playerNr);
    int getPlayerCount() const;
    int getHoleCount() const;
    bool hasHole(std::string holeName);
    void addHole(sd::Hole hole);
    void editHole(int holeNr);
    bool isActive();
    bool isPlayable();
    bool isPlayed();

};


} //End of namespace sd
#endif

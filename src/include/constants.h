#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <string>

const unsigned int c_maxNameLen = 25;
const std::string c_saveDir = "savedata";
const std::string c_fileSuffix = ".mgf";
const std::string c_holesFile = "holes_save_data.dta";


#endif
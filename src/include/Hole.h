#ifndef Hole_H
#define Hole_H

#include <string>

namespace sd{

/**
* Class with basic data about a Hole
*/

class Hole{
private:
    std::string m_name;
    int m_par;
    //int m_trackNr;

public:
    Hole();
    Hole(std::string name, int par);
    void setName(const std::string &newName, const int &maxLen);
    std::string getName()const;
    void setPar(const int &banePar);
    int getPar()const;
    //void setTrackNr(const int &trackNr);
    //int getTrackNr()const;

};

}




#endif // Track_hole_H

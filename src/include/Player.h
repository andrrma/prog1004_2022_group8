#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <vector>

namespace sd{

/**
 * Class with basic data about a single golf player
 */
class Player {
private:
    std::string m_name;
    //int m_playerNr;
    //int m_teamNr;
    //int m_handicap;
    std::vector<int> m_score;

public:
    Player();
    Player(const int &tracks, const std::string name);
    void setName(const std::string &newName, const int &maxLen);
    std::string getName() const;
    //void setPlayerNr(const int &playerNr);
    //int getPlayerNr() const;
    //void setTeamNr(const int &teamNr);
    //int getTeamNr() const;
    //void setHandicap(const int &handicap);
    //int getHandicap() const;
    void setStroke(const int &stroke, const int &trackNr);
    int getStroke(const int &trackNr) const;
    void updateTrackCount(const int &trackNr);
};

}
#endif
#ifndef TEAMS_H
#define TEAMS_H

#include "Player.h"

#include <string>
#include <vector>


namespace sd {

/**
 * Class containing all players for one mini-golf team.
 */
class Team {
private:
    std::string m_name;
    std::vector<int> m_playerIndex;
    

public:
/*
    Team() {};
    void setName(std::string name, const int &maxLen);
    std::string getName() const;
    void addPlayerID(Player* newPlayer);
    void editPlayer(Player* player);
    void removePlayer(const int &playerID);
    Player* getPlayerList(std::string player) const;
*/
};
}

#endif
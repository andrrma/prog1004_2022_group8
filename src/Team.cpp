#include "include/Team.h"
#include "include/Utility.h"
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <cctype>
#include <cstring>

const int MAXNAMELEN = 50;

namespace sd {

//*****************************************************************************
// Team object is currently not in use, but is left intact for future
// implementation of team play.
//*****************************************************************************


/*
void Team::setName(std::string name, const int &maxLen) {
    if(name.length() <= maxLen) {
        if(!hasSymbol(name, true)) {
            m_name = name;
        } else {
            std::cout << "Error: Name contains illegal characters\n";
        }
    } else {
        std::cout << "Error: Name too long.\n";
    }
}

std::string Team::getName() const {
    return m_name;
}


void Team::addPlayer(Player* newPlayer) {
    m_players.push_back(newPlayer);
}


void Team::editPlayer(Player* player) {
    for(Player* p : m_players) {
        if(p == player) {
            //Edit player parameters here.
        } else {
            std::cout << "Player not found.\n";
        }
    }
}


void Team::removePlayer(Player* player) {
    for (int i = 0; i < m_players.size(); i++) {
        if (m_players[i] == player) {
            m_players.erase(m_players.begin() + i);
        }
    }
}


/**
 * Returns a pointer to a player if a match is found, or a nullptr.
 * 
 * @param player - name or number.
 * @return Player*  or nullptr.
 */
/*
Player* Team::getPlayerList(std::string player) const {
    
    player = trimWhitespace(player);
    if (isNumber(player)) {
        int nr = atoi(player.c_str());
        if (0 <= nr && nr <= 2147483647) {
            for (Player* p : m_players) {
                if(nr == p->getPlayerNr()) {
                    return p;
                }
            }
        }
        return nullptr;
    } else if (player.size() <= MAXNAMELEN) {
        for (Player* p : m_players) {
            if(p->getName() == player) {
                return p;
            }
        }
        return nullptr;
    } else {
        return nullptr;
    }
}
*/
}


    
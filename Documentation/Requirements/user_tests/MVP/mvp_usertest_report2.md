# Usability test of MVP: Report

[[_TOC_]]

## Purpose

The purpose of the test was to assess the functionality of the MVP, focused on the following aspects:

- Validating the core functions of the program and potentially discover underlying flaws in the underlying code.
- Observe how someone with little understanding of the program interact with the command-line interface of the program (ease of use / intuitivity).

## Methodology

Participants engaged in one-on-one test and interview sessions with our testers, first being administered a short batch of specific tasks to be performed in the interactive prototype while they were encouraged to verbalize any doubts they had about functionality. Observations were noted down, then after the tests were completed, they were followed up with a batch of questions regarding the user's interaction with the program.

Observations and reformulated answers to questions were given concise, simplified wording and then sorted in groupings where similar observations grouped together when possible. These groups and remaining single observations were then reformulated into problem statements that could be addressed in this report. For insight into the raw test data, see the wiki, as this report only will look at condensed problem statements.

<br>

---

## Problem Statements and Solutions

---

### 'N: Name:' Menu Feature

<table>
<tr>
<th>

_The “N: Name:” menu option to add/edit names wasn’t explicit enough in its purpose and functionality. Gut reaction was to try inputting name instead of inputting 'N', and it's functionality for editing the name wasn't obvious._
</th>
</tr>
</table>

Several users found the way the program handled input for a player/hole/tournament's name was confusing. Repeating the action of inputting 'N' would allow you to overwrite the old name already registered, but users did not find this obvious or intuitive.

<br>

**Solution:**

we found the presence of ':' right after N triggered a response where the user would assume the program was expecting an input. Changing it to 'N - Name:' made it read more clearly as a menu option and not a prompt expecting an input. We also added contextual awareness to the menu display, having the menu print 'M - Edit Name:' in the event a name was already registered by the user to make the double functionality explicit to the user.

<br>

---

### Registering stroke by Current Hole

<table>
<tr>
<th>

_Users felt registration of strokes by “Current Hole” was confusing, not communicating how it worked to the user._
</th>
</tr>
</table>

Registering strokes by player currently takes the player through one hole at a time in order, cycling to the next hole when a stroke is registered for all players for the previous one. This is likely fine for an experienced user, but not user friendly for new users.

<br>

**Solution:**

NB! This problem has as of yet to be addressed in the latest version due to time constraints.

An obvious solution is to have the program communicate more clearly the functionality to the user during input. Adding more flexibility to the order of stroke input would also be a good idea, such as giving the control direct control over when to move on to the next hole as the automation, while practical to someone who knows its operation, is unintuitive as it stands right now for a new user.

<br>

---

### Creating New Tournament

<table>
<tr>
<th>

_Users found creating a new tournament confusing due to a lack of a visual on already registered players and added holes._
</th>
</tr>
</table>



<br>

**Solution:**

This issue has been addressed by adding the option to view all registered players and holes in a current tournament upon request. We've chosen not to print it to console by default with the menu to avoid visual clutter.

<br>

---

### Negative Indicator for Stroke Under Par (score)

<table>
<tr>
<th>

_Stroke under par being displayed as a negative digit was confusing to some unfamiliar with golf rules._
</th>
</tr>
</table>



<br>

**Solution:**

NB! This issue has as of yet to be addressed.

This is a problem likely only to appear with users not familiar with the rules of golf, but in the future we will add clarifying text, provided the text doesn't clutter the screen to the point where it comes at the cost of clarity. For now we've chosen to down prioritize this problem due to implementation of other functions taking precedence, as well as it being a non-issue for the software's intended user-group.

<br>

---

### Returning to Menu Root

<table>
<tr>
<th>

_Users found returning to the main menu or other previous menu branches cumbersome due to the back option often changing lettering._
</th>
</tr>
</table>

As the menu is operated with the input of letters, users felt it was awkward to moving backwards through menu branches. Frequently the return option would be named "B - Back", "R - Return" or similar.

<br>

**Solution:**

We've solved this issue in tandem with the problem below. By adding breadcrumbs along the top of the menu in gray text we give the user information about where they are in regards to menu depth at any given time without the text taking away their focus. White text was too noisy. Formatting it like directories with underscores and lowercase also helped taking focus away from it. "/main_menu/new_tournament" vs Main "Menu Menu/New Tournament", for instance. The former is largely ignored while the latter had a tendency to register mentally as a menu item.

<br>

---

### Lost in Menu Depth

<table>
<tr>
<th>

_The user at times felt lost as to how deep they were in the menu system._
</th>
</tr>
</table>

<br>

**Solution:**

We've solved this issue in tandem with the problem above. Since we already implemented breadcrumbs it was practical to use the wording from the container of the breadcrumbs to dynamically alter the wording of the 'B - Back' feature. Returning up a level is now always named 'B - Back to' followed by the name of the menu a level up in the menu hierarchy, i.e 'B - Back to main menu' or 'B - Back to manage holes'.

<br>

---

### Lack of Player/Hole Overview During Play

<table>
<tr>
<th>

_Users found the lack of any display of registered players participating and holes in an active tournament confusing._
</th>
</tr>
</table>

<br>

**Solution:**

This problem has been solved using the functionality implemented to solve the earlier problem 'Creating New Tournament', namely implementing a function allowing the user to print all participants and holes to the screen on command.

<br>

---

## Final Notes

As the MVP was lacking quite a few features, we've taken lessons learned from this and the previous user test and applied the solutions to other similar features implemented in the latest iteration of the EasyPut software.

We've tried to use contextually aware menus where practical, never displaying menu options the user cannot utilize, only having them appear when they're relevant to the progress of the user. This was done based on the fact that the function of a menu item often can be ambiguous to users if they're too similarly named. As such we've made menu options mutate naming and behavior depending on context to make function explicit for its purpose in the current context, in addition to hiding irrelevant ones from the user.

Long names create visual noise, and so we've chosen to reduce on screen menu items as much as possible to better distinguish between the relevant ones visually.

We've found we're always walking a fine line when it comes to wording, formatting and positioning on screen when it comes to readability and perception of intent when it comes to a text only interface, and strive to work around these limitations in the final product.
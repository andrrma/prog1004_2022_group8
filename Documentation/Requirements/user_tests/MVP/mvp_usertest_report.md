# **Usability test of MVP: Summary**



## **Purpose**

The purpose of the test was to assess the functionality of the MVP, focused on the following aspects:

- Validating the core functions of the program and potentially discover underlying flaws in the underlying code.
- Observe how someone with little understanding of the program interact with the command-line interface of the program (ease of use / intuitivity).

<br>

**Methodology**

Participants engaged in one-on-one test and interview sessions with our testers, first being administered a short batch of specific tasks to be performed in the interactive prototype while they were encouraged to verbalize any doubts they had about functionality. Observations were noted down, then after the tests were completed, they were followed up with a batch of questions regarding the user&#39;s interaction with the program.

Observations and reformulated answers to questions were given concise, simplified wording and then sorted in groupings where similar observations grouped together when possible. These groups and remaining single observerations were then reformulated into problem statements that could be adressed in this report. For insight into the raw test data, see the test summary [ ], as this report only will look at condensed problem statments.


<br>
<br>


# Problem Statements and solutions

Going through the observations and answers to questions, we&#39;ve reduced each one to a problem statement that can be addressed, loosely grouped within categories. For each one, solutions should be filled in and voted on by project team members before beginning work on the next iteration.

| **U2.OB1**| **Had a bit of problem moving trough the menu's at first** |
| --- | --- |
| **S1** | _Add a point to the User-manual that explains how the CLI works for Users who have little experience with CLI_ |


**setting/changing player name**

| **U1.OB1 / U3.OB2 / U3.OB5** | **The "N: Name:" menu option wasnt explicit enough in purpose and functionality ** |
| --- | --- |
| **S1** | _Rewrite "N: Name" to "N - Name"._ |
| **S2** | _Make name change depending on the context to "Edit Name"._ |

**Registering strokes / Holes**

| **U1.OB3 / U3.OB4**  | **Registration of strokes by "Current Hole" was confusing, not communicating how it worked to the user.** |
| --- | --- |
| **S1** | _Add communicative text to better explain what is going on_ |
| **S2** | _Focus on explaining the functionality in the user-manual in case the user still struggles after getting feedback from the programm_ |

**Creating New Tournament**
| **U3.OB4 / U2.OB3** | **Lack of information about already registered players and holes were missing and made creating a new tournament confusing** |
| --- | --- |
| **S1** | _Add the option to display and edit already registered players and holes in the tournament._ |


**Ruleset**
| **U2.OB4** | **Confused with why it said "Score: -1" since they were not familiar with the rule / ruleset.** |
| --- | --- |
| **S1** | _Indicate the meaning with plaintext._ |

**Menu system**
| **U3.OB6** | **Felt lost in regards to how deep they were in the Menu System.** |
| --- | --- |
| **S1** | _Add "Breadcrumbs" along the top of the screen._ |

| **U3.OB4** | **Quick path back to the main menu without having to go back X amount of pages, especially since the "back buttons" often changed lettering** |
| --- | --- |
| **S1** | _Streamline the "back buttons" and have them use the breadcrumbs for naming_ |

| **U2.OB5 / U2.OB3** | **Lack of displaying registered holes and players in active tournament** |
| --- | --- |
| **S1** | _Add feature to view players and holes outside of registration of score._ |



## **Usability Test Retrospective:**

## **What can be done better for later tests?** 

- Take better notes of what the user is struggling with, even if it is just supposed to be an observation, ask the user what they're struggling with in the moment. In that way we get more in-depth information.
- If possible, get hold of someone with experience managing tournaments to test the prototype.
- Once the complexity of the prototype grows, more complex tasks should be given to further test readability of menu and feedback from the program.

## **Collected DATA From MVP User Test**

This is a summary of all data from the user test of our MVP. A report detailing condensed problem statements and sollutions is provided separately.


<br>

**Participants:**

Each user is anonymous, but some basic questions about technological proficiency have been asked to establish a frame of reference for observations done during the tests, along with age bracket and gender to give a better picture of who our test subjects represent.

| **User 1**   |   |
| --- | --- |
| Age bracket  | 18-25 |
| Sex  | Female |
| Proficiency with technology  | no experience with coding or CLI, basic computer and smartphone |
| Experience with console-based software  | None |
| Experience with managing tournaments  | None |

<br>
<br>

| **User 2**   |   |
| --- | --- |
| Age bracket  | 50-60 |
| Sex  | Female |
| Proficiency with technology  | Simple use of PC and Phones |
| Experience with console-based software  | None |
| Experience with managing tournaments  | None |

<br>
<br>

| **User 3**   |   |
| --- | --- |
| Age bracket  | 20-25 |
| Sex  | Male |
| Proficiency with technology  | some experience with coding, and also other everyday technology |
| Experience with console-based software  | minimal |
| Experience with managing tournaments  | None |


<br>
<br>

### Tasks and Observations

Observations indicating useful data given an observation number, such as U1.OB1 indicating it is the first observation for user 1.

<br>

| **Task 1**   |  Initiate the registration of a new tournament, a new player, add holes, and then finally finalize the tournament and return to the main menu. |
| --- | --- |
| **U1**   | Completed the task without any major difficulty |
| **U2.OB1** | Did it pretty well. Had a bit of problem moving trough the menu's at first but when she got it. It became much easier |
| **U3.OB1** | The user performed the task without much trouble. Did however struggle a bit with adding a player. |


<br>
<br>


| **Task 2**   | initiate another tournament and add a player. Let’s say you inputted the wrong name, try to change the name, (don't finalize the tournament, just return to main menu). |
| --- | --- |
| **U1.OB1** | Thought she would make another player when changing the name but it went up for her that it was the same player when she tried it out |
| **U2** | Managed to change name when she had wrong input at first. |
| **U3.OB2** | After some thought, the user figured out how to change the name. But he did say that it would be more intuitive if "Name" changed to "Change name:" after the first input. |


<br>
<br>

| **Task 3**   | Register strokes for all players on all holes |
| --- | --- |
| **U1.OB2** | went to manage holes first, then went to active tournament and did it correctly without trouble |
| **U2** | Managed to register strokes well |
| **U3.OB3** | Figured it out relatively quickly, but wished there was some indication as to how many players/holes were needed to start inputting strokes. |

<br>
<br>

| **Task 4**   |  Locate the score for all players |
| --- | --- |
| **U1**   | No difficulties performing the task |
| **U2** | Did that well to. |
| **U3**   | The user easily navigated to the right option. |


<br>
<br>

| **Task 5**   |  Add a new hole, delete another hole and then check the list of holes |
| --- | --- |
| **U1**   | no problems at all |
| **U2.OB2** | Went to active tournament instead of manage holes because I think she thought active tournament managed every thing about current tournament |
| **U3**   | The user had no problems adding/deleting a hole. |


<br>
<br>

| **Task 6**   | Have them create a new tournament, add at least 2 players, at least 1 holes, register stroke for all players on all holes, finalize the tournament and exit the entire program |
| --- | --- |
| **U1**   | Powered through the entire task, no problems |
| **U2** | Did almost great didn't remember to finalize one of the players |
| **U3**   | After learning the layout of the program from the previous tasks, the user figured out the logic, and completed the task without trouble. |


<br>
<br>



### Q&A



| **Q1**   |  What was the most difficult / confusing Task to complete |
| --- | --- |
| **U1.OB3** | when she had more than one hole, she was not sure how to register the strokes for all the holes, can she choose current hole again for the next hole, how often can she hit current hole / when does it update to the next hole |
| **U2.OB3** | Didn't get a view/list of current players and tracks in current tournament |
| **U3.OB4** | "task 3 was the most confusing as I did not find it intuitive, what number of players/holes was needed to update the score." |


<br>
<br>

| **Q2**   |  What part of the menu was the least intuitive? |
| --- | --- |
| **U1** | None, letters were logical (m - main menu, p - add player) |
| **U2**   | No answer |
| **U3.OB5** | "The least intuitive thing about the menu was when adding player names, and how I had to press option N twice for it to start the prompt" |


<br>
<br>

| **Q3**   |  What part of the menu did you find the most intuitive? |
| --- | --- |
| **U1** | all pretty equal |
| **U2** | No answer |
| **U3** | "The menu for managing holes was the most intuitive, as it was easy to imagine what the output would be for each option" |


<br>
<br>

| **Q4**   |  Can you tell me what you think of the way the displayed data was structured? |
| --- | --- |
| **U1** | in the score for the players the table was nicely done, easily readable. number of holes was nicely listed and structured, could find everything she needed |
| **U2.OB4** | On display score she didn't new the rule so she didn't know why it stood -1 |
| **U3.OB6** | "I sometimes found myself to be a little lost in the depth of the menus. The displayed data was fine, but sometimes i did not realise just how far in i was, in regards to the menu system." |


<br>
<br>

| **Q5** |  What would you say is the difficulty for learning the program? Scale 1-10 and why? |
| --- | --- |
| **U1** | 1, thought it was explained well, "everything i needed was listed", "data will be lost, are you sure?", displayed what would happen when a command / action was done, every option was nicely listed and logical |
| **U2**   | No answer |
| **U3**   | "5. I needed a little time to get used to the logic behind the program, but after that it was fine." |


<br>
<br>

| **Q6**   | What changes would you like to see made to the program? (keep in mind the program has to be CLI) |
| --- | --- |
| **U1.OB4** | quick path to main menu without having to go back 4 pages |
| **U2.OB5** | A menu option to view players and tracks in the tournament so its easier to change. |
| **U3**   | "In the New Tournament menu, i wished the N option said: Tournament Name, instead of just name. Similarly, when adding a player, I wished the program automatically expected a name input. I also would have liked it if, after finalizing a tournament, the final scoreboard was printed" |


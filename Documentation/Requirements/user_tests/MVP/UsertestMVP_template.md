**Usertest: Iteration 2**

**Usability test of MVP**

**Purpose**

The purpose of this test is to assess the functionality of the MVP, focused on the following aspects:

- Validating the core functions of the program and potentially discover underlying flaws in the underlying code.
- Observe how someone with little understanding of the program interact with the command-line interface of the program (ease of use / intuitivity).

**Methodology**

Participants engaged in one-on-one test and interview sessions with our testers, first being administered a short batch of specific tasks to be performed in the interactive prototype while they were encouraged to verbalize any doubts they had about functionality. Observations were noted down, then after the tests were completed, they were followed up with a batch of questions regarding the user&#39;s interaction with the program.

Observations and answers to questions are then reformulated to problem statements that can be addressed in the next iteration of the product prototype.

**Participants:**

| **Participant**   |   |
| --- | --- |
| Age bracket  | |
| Sex  |   |
| Proficiency with technology  | |
| Experience with console-based software  | |
| Experience with managing tournaments  |

**Tasks and Observations:**

Observations indicating useful data colored in light orange and given a number.

| **Task 1**   |  Initiate the registration of a new tournament, a new player, add holes, and then finally finalize the tournament and return to the main menu. |
| --- | --- |
| **Observation**   | |

| **Task 2**   |  initiate another tournament and add a player. Let’s say you inputted the wrong name, try to change the name, (dont finalize the tournament, just return to main menu). |
| --- | --- |
| **Observation**   | |


| **Task 3**   |  Register strokes for all players on all holes |
| --- | --- |
| **Observation**   | |

| **Task 4**   |  Locate the score for all players |
| --- | --- |
| **Observation**   | |

| **Task 5**   |  Add a new hole, delete another hole and then check the list of holes |
| --- | --- |
| **Observation**   | |

| **Task 6**   |  Have them create a new tournament, add at least 2 players, at least 1 holes, register stroke for all players on all holes, finalize the tournament and exit the entire program |
| --- | --- |
| **Observation**   | |

**Q&amp;A:**

Answers with useful data colored in light orange.

| **Q1**   |  What was the most difficult / confusing Task to complete |
| --- | --- |
| **Answer** | |

| **Q2**   |  What part of the menu was the least intuitive? |
| --- | --- |
| **Answer** | |

| **Q3**   |  What part of the menu did you find the most intuitive? |
| --- | --- |
| **Answer** | |

| **Q4**   |  Can you tell me what you think of the way the displayed data was structured? |
| --- | --- |
| **Answer** | |

| **Q5** |  What would you say is the difficulty for learning the program? Scale 1-10 and why? |
| --- | --- |
| **Answer** | |

| **Q6**   |  What changes would you like to see made to the program? (keep in mind the program has to be CLI)  |
| --- | --- |
| **Answer** | |

**Problem statements and Solutions**

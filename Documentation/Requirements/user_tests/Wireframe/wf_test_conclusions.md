## **Improvements for wireframe**

- change layout for the main menu ("manage tracks instead of administrate tracks)
- change from "administrate tournaments" to "Active tournament" and put finalized tournaments another place.
- on update score, add _for_ individual player / _for_ current hole
- remove display results from main menu and have put it in active tournament instead
- implement ESC as a way to cancel input
- change sequential data input to a menu style
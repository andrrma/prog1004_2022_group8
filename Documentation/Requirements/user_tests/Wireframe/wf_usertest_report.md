## **Problem statements and Solutions**


Going through the observations and answers to questions, we&#39;ve reduced each one to a problem statement that can be addressed, loosely grouped within categories. For each one, solutions should be filled in and voted on by project team members before beginning work on the next iteration.

**Location of features was confusing**

| **U1.OB1** | **User was confused on where to add new tracks** |
| --- | --- |
| **S1** | _Change the wording of the feature for managing tracks to be more explicit, such as &quot;Edit Tracks&quot;_ |
| **S2**| _Change the wording to &quot;Manage Tracks&quot;_|
| **S3**| _Show possible options in parentheses_ |
| **S4**| _Change wording to manage instead of administrate._|

| **U1.OB2** | **User was confused as to where one would register player score.** |
| --- | --- |
| **S1** | _preface options with 'for'_ |
| **S2** | _Add help menu_ |
| **S3**| _Include &quot;(Edit score)&quot;_ |

| **U1.OB5** | &#39; **Display Results&#39; were found both in the main menu and under &#39;Administrate Tournament&#39;** |
| --- | --- |
| **S1** | _remove / move the double occurence_ |
| **S2**|  _Rename display results in main menu to &quot;show most recent results&quot;_ |
| **S3**| _Remove option from &quot;Administrate tournament&quot;_ |

**Unclear what was expected of user to perform a function**

| **U1.OB1** | **User was unsure of expected input format (string vs integer).** |
| --- | --- |
| **S1** | _Add info about possible input_ |
| **S2**| _Write a short example to show what the expected input is_ |


**Misleading menu item name**

| **U1.OB3** | **User was distracted by “Display Results”, thinking he could register player score there..** |
| --- | --- |
| **S1** | _Same as before, move element so that intent is clear_ |
| **S2**| _Rename or move function_ |

**User not finding desired functionality**

| **U4.OB1** | **User found no way to cancel updating score.** |
| --- | --- |
| **S1** | _Feature is there, but easy to miss. Implement more prominent alternative_ |
| **S3**| _Change method to cancel_ |
| **S4**| _Make it clearer that 0 means cancel, or implement &quot;esc&quot; as an option_ |

| **U1.OB4** | **The user would like to be able to cancel input of a player&#39;s score.** |
| --- | --- |
| **S1** | _Feature is there, but easy to miss. Implement more prominent alternative_ |
| **S2**| _Add extra step to cancel_ |


| **U1.OB6** | **User found no option to edit a single parameter for a user while inputting user data.** |
| --- | --- |
| **S1** | _Change sequential data input to a dedicated menu_ |
| **S2** | _Add a menu for choosing what to edit_ |

<br>
<br>
<br>

---
## **Usability Test Retrospective:**

## **What can be done better for later tests?** 

- Spend more time formulating questions that cannot be answered with simple yes/no answers.
- Put more thought into how to get qualitative data from tests and questions before performing tests.
- If possible, get hold of someone with experience managing tournaments to test the prototype.
- Once the complexity of the prototype grows, more complex tasks should be given to further test readability of menu and feedback from the program.
- Avoid sticking strictly to questions and accepting basic answers without probing further.

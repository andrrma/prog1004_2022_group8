## **Report: Wireframe Iteration 1**

# **Usability test of Interactive Wireframe Prototype**



**Purpose**

The purpose of this test is to assess the usability of the prototype&#39;s interface, focused on the following aspects:

- **Visibility** : Are menu items placed in accordance with their importance, and is the wording of each item appropriate for its function?
- **Feedback** : Is the user given sufficient information as to what is happening when they input data or make mistakes?
- **Consistency** : Does moving through menus and inputting user data follow a consistent pattern such that the user does not get confused by changes in how the program operates?
- **Error handling** : If the user inputs erroneous data, does the program provide sufficient and appropriate leeway to change the data after the mistake is done?

**Methodology**

Participants engaged in one-on-one test and interview sessions with our testers, first being administered a short batch of specific tasks to be performed in the interactive prototype while they were encouraged to verbalize any doubts they had about functionality. Observations were noted down, then after the tests were completed, they were followed up with a batch of questions regarding the user&#39;s interaction with the program.

Observations and answers to questions are then reformulated to problem statements that can be addressed in the next iteration of the product prototype.

<br>
<br>



# **Participants:**

Users have been asked some basic questions about their proficiency when it comes to handling technology, console programs, and their experience managing tournaments.

<br>

| **User 1** | |
| --- | --- |
| Age bracket | 30 - 35 |
| Sex | Male |
| Proficiency with technology | He has worked within IKT support, has a high degree of proficiency with everyday technology. |
| Experience with console-based software | Long time user of Linux command line and experience interacting with software through terminal interfaces. |
| Experience with managing tournaments | No experience managing tournaments |

| **User 2** | |
| --- | --- |
| Age bracket | 18 - 25 |
| Sex | Female |
| Proficiency with technology | Basic use of computers I.e., web-browser, basic games, and simple applications like Spotify. |
| Experience with console-based software | None. |
| Experience with managing tournaments | Very little. |

| **User 3** | |
|:---------|:---------------------------------------------------|
| Age bracket |55-65 |
| Sex |Male |
| Proficiency with technology |very bad|
| Experience with console-based software | none|
| Experience with managing tournaments |small tournament|

| **User 4** ||
| --- | --- |
| Age bracket | 18-25 |
| Sex | Female |
| Proficiency with technology | Basic knowledge of computers and everyday technology. No knowledge of programming. |
| Experience with console-based software | None. |
| Experience with managing tournaments | Some small tournaments up to 15 people. |

| **User 5** | |
|---|---|
| Age bracket | 18 – 25. |
| Sex | Male. |
| Proficiency with technology | Basic understanding. Some knowledge in programming, and other every-day technology. |
| Experience with console-based software | None. |
| Experience with managing tournaments | Very little. |

<br>
<br>
<br>

# **Tasks and Observations:**

Tasks and corresponding observations are presented below. Any observations done during the tasks are written below the description of the task, labeled U1 through U5 indicating the user the observation relates to. Observations with important data are further labeled with OB and the observation number for that user. i.e U1.OB1.
<br>
<br>

| **Task 1** | _Initiate the registration of a new tournament, add a new team, a new player, add tracks, and then finally finalize the tournament and return to the main menu._|
| --- | --- |
| **U1.OB1** | Asked if team choice for player registration expected a string or team number as input, but otherwise he seemed comfortable navigating through the menu and executing the given instructions. |
| **U2** | Performed the task without any problems (had to get used to how Balsamiq operated, but irrelevant). |
| **U3** | User handled it well and didn&#39;t have any problems |
| **U4** | The user had no problems performing the task. |
| **U5.OB1** | Performed the task without too much trouble. Some confusion on adding new tracks. |

| **Task 2** | _Update the score for the current track, then do the same for a specific player. Ask them to imagine that they made an error in inputting the players actual score and have them try to &quot;fix&quot; the error. Finally, finalize the tournament and return to the main menu._|
| --- | --- |
| **U1.OB2** | Displayed slight confusion at where the function for registering score would be located. Checked out &#39;administrate tracks&#39; before he looked at &#39;administrate tournament.&#39; |
| **U1.OB3** | He said &#39;Display Results&#39; distracted him from the &#39;Administrate Tournament option&#39; where the option to register score is located. |
| **U1.OB4** | User remarked he would have liked to be able to cancel the input of the results for a single player. |
| **U2** | Performed the task well, completed it without any major difficulties |
| **U3** | Same as task 1 |
| **U4.OB1** | Performed the task with no problems but wished there were a way to cancel updating score. |
| **U5** | After a little time spent comprehending the task, the user had no real issue with completing the task. |

| **Task 3** | _Find a way to display the results for all the competing teams, then do the same for individual players._ |
| --- | --- |
| **U1** | The user had no issues locating each functionality and was able to rapidly perform the given task. |
| **U2** | Completed without any problems |
| **U3** | Did manage that pretty well |
| **U4** | Completed with no issues. |
| **U5** | They showed no confusion with the task and managed to perform it quite easily. |

| **Task 4** | _Find a way to display all stored tracks_ |
| --- | --- |
| **U1** | The user was able to locate the functionality without issue. |
| **U2** | Completed without any problems |
| **U3** | Good |
| **U4** | Completed with no issues. |
| **U5** | No issues regarding this task. |

| **Task 5** | _First ask the user to add a new track to the system. Afterwards, have them delete a track from the system, then return to the main menu._|
| --- | --- |
| **U1** | The user executed the given task efficiently and without problem. |
| **U2** | Completed without any problems |
| **U3** | Did manage that pretty well |
| **U4** | Completed with no issues. |
| **U5** | The user had no problem with this task. |

| **Task 6** | _Ask the user to freely roam through the program and explore. Finally, have them quit the program._ |
| --- | --- |
| **U1** | The user at this point seemed quite comfortable maneuvering through the program interface. |
| **U2** | Completed without any problems |
| **U3** | Did manage that pretty well |
| **U4** | The user navigated the program easily with no issues or additional comments. |
| **U5** | After getting used to Balsamiq, they seemed comfortable with how the program worked. They started to get a feel for the program layout itself. |

<br>
<br>
<br>

# **Questions and Answers:**

Questions and corresponding answers are presented below. Any questions with valuable data are given a numbered OB tag like in the task section.

<br>

| **Q1** | _Did you feel there were any features that were missing from the program?_ |
| ---| --- |
| **U1** | &quot;Al seemed to be in order.&quot; |
| **U2** | &quot;No, not really.&quot; |
| **U3** | The user wanted side numbers to have track which side the user was on. |
| **U4** | &quot;No main features were missing, but I wish I had the option to cancel updating information.&quot; |
| **U5** | &quot;I feel the current features are sufficient.&quot; |


| **Q2** | _Did you find yourself in a situation where the feedback from the program was confusing?_|
| --- | --- |
| **U1** | &quot;Not really, the amount of feedback seemed fitting for the tasks being performed.&quot; |
| **U2** | &quot;No, it said what had been done, after I did it.&quot; |
| **U3** | &quot;Nothing&quot; |
| **U4** | &quot;Nothing was confusing.&quot; |
| **U5** | &quot;No, the feedback i got correlated with what i expected.&quot; |

| **Q3** | _Did you experience the program behaving in an unexpected manner, or did executing an option yield a surprising result?_ |
| --- | --- |
| **U1.OB5** | &quot;For the most part naming of menu items and executing them yielded the expected result aside the location of the &#39;Display Results&#39; option confusing me a bit as mentioned earlier. The same option was found under &#39;Administrate Tournament&#39; as well and was more at home there.&quot; |
| **U2** | &quot;No.&quot; |
| **U3** | &quot;No&quot; |
| **U4** | &quot;No unexpected behavior.&quot; |
| **U5** | &quot;No, nothing really surprised me.&quot; |

| **Q4** | _Did you feel the program was/would be punishing in any way if you input the wrong data, and if so, what features do you think could help alleviate it?_ |
| --- | --- |
| **U1.OB6** | &quot;No, the program asks for confirmation frequently enough that it didn&#39;t seem like it would have been an issue, though editing user information could have been an option so you wouldn&#39;t have to redo all the inputs.&quot; |
| **U2** | &quot;Was easy to correct the data.&quot; |
| **U3** | &quot;Not that I was aware of&quot; |
| **U4** | &quot;No, correcting the data was easy.&quot; |
| **U5** | &quot;No, i did not think much of it.&quot; |

| **Q5** | _Was it ever unclear where you were in the program or what you were supposed to do?_|
| --- | --- |
| **U1** | &quot;I didn&#39;t have any problems knowing where I was in the process, aside being confused at where I&#39;d find a few things at first, like mentioned earlier.&quot; |
| **U2** | &quot;No.&quot; |
| **U3** | &quot;No.&quot; |
| **U4** | &quot;I had no problems understanding where I was or what I was supposed to do.&quot; |
| **U5** | &quot;After taking some time to fully comprehend the program, there was at no point unclear where i was.&quot; |

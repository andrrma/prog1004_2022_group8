**Vision document**

Program: EasyPut

Group 8

# Contents
[[_TOC_]]

# 1.             Introduction

The purpose of this document is to collect, analyze, and define high-level needs and features of the program &quot;EasyPut&quot;. It focuses on the capabilities needed by the users, and **why** these needs exist. The details of how &quot;EasyPut&quot; fulfills these needs are detailed in the use-case and supplementary specifications.

## 1.1           Purpose and scope

The purpose of this document is to give an overview of the process and goals of the project. Like risks we can encounter when we are doing the project. We also have project goals, impact goals, result goals and process goals. This will help us to make a plan on what we want to achieve through the different phases of the project. This document as well includes preparation before starting the project. For example cost, expenses and features of the project.

## 1.2           Definitions, Acronyms, and Abbreviations

This subsection provides the definitions of all terms, acronyms, and abbreviations required to properly interpret the **Vision** document. This information may be provided by reference to the project&#39;s Glossary.

## 1.3           References

This subsection provides a complete list of all documents referenced elsewhere in the **Vision** document. Identify each document by title, report number if applicable, date, and publishing organization. Specify the sources from which the references can be obtained. This information may be provided by reference to an appendix or to another document.

## 1.4           Overview

The document is organized with an introduction, then it is a bit of where the project can be useful and what our goals with the project is. Then it goes more into the process and what problems etc. can happen. And in the end of the document there is what we need to support the project like use manual and more.

# 2.             Positioning

This section will duscuss the "position" of our company, what the market is like, what business oppurtunities are related to this project and ultimately what problems are beeing solved by us developing this product.

## 2.1           Business Opportunity

It&#39;s a small market, but it is a good opportunity to become a big operator in the mini-golf tournament industry. Mini golf is a sport that is played in almost every country.

## 2.2           Problem Statement

| The problem of |            Recording tournament results by hand |
| --- | --- |
| affects | Tournament managers |
| the impact of which is | Reduced efficiency, higher costs |
| a successful solution would be | Digitalization of results, less cluster, lower costs, and higher efficiency. |

## 2.3           Product Position Statement

| For | Mini-golf companies  |
| --- | --- |
| Who | Needs a way to manage tournaments |
| The (product name) | EasyPut |
| That | Easy to handle and to a good price |
| Unlike | primary competitive alternative |
| Our product | Our product is a program for making a mini-golf tournament      |

A product position statement communicates the intent of the application and the importance of the project to all concerned personnel.

# 3.             Project goals

This section describes the short, and long term goals of this project/program. It is divided into three different sub-sections, each describing a different aspect of the intention regarding our end product. 

## 3.1           Impact goals

•	Streamlining the process of tournament organizing

Helps reduce time usage on otherwise tedious tasks

Helps improve productivity

•	Less need for manual labour 

Helps reduce the time spent on planning 

Lowers labour costs

•	Easier to log records of tournaments

Eliminates the need for pen and paper

Helps with overall orderliness

•	Better customer service 

Helps with the overall perception and reputation of the company

•	Reduced long term costs

Helps finance other business expenses


## 3.2           Result goals

The result goal of this project is mainly to create an application meant for a mini-golf tournament organizer. This is meant to be an easier solution to the aspect of organizing tournaments, while also making it as user-friendly as possible. However, for the project itself, the goal is to keep high standards regarding the documentation required, so as the final delivery will resemble what may be expected from a modern day workplace. 

## 3.3           Process goals

1. Become acquainted, build trust in each other, and increase motivation for the study

2. We want to hand in a high-quality project.

3. We want to learn the contents of the course and have a better understanding of
developing a project as a team.

4. We will facilitate collaboration to maximize team effectiveness.

5. Distribute roles and utilize them in the best possible way, while however being
flexible and take on other roles as necessary.

- This is, to increase team development and highlight teamwork for
the duration of the project.

# 4.             Product Overview

This section will provide a overview of the program in the context of its capabilities, system configurations and weather or not its dependent on other applications or interfaces to function at the inteded level. In addition, this section will also quantify and give examples to the benfefits related with using our product, and also what risk's are involved with this project along with the estimated cost of this project.

## 4.1           Product Perspective

The product is self-contained and is not a subsystem dependent on a larger system to function as intended. All the files that are required to have the program work properly, including files for saved tournaments and holes, will be stored locally and is acessable to the user. Should you need to transfer tournaments or other data (like saved holes) to another computer, the save-files can easily be copy-pasted into the corresponding folder on another computer. note that the naming for the files has to stay unchanged, and therefore you can only have one file containing the holes and only one file containing the tournaments for the program.

## 4.2           Summary of Capabilities

| **Customer Benefit** | **Supporting Features** |
| --- | --- |
| Reduce the need for pen and paper | A fully digital system to manage and document tournaments. |
| More effective logging of tournaments. | A digital system reduces the time spent when setting up a tournament. |
| Easier for the players to see their match history. | Having all the documented tournaments digitally makes it easier for players to see the match history compared to having it all on paper. | 

## 4.3 Assumptions and Dependencies
The user must have have a windows operating system to run the program. The user also need a pc that is functional to run the program. Because if the pc is unstable it can affect the performance of the program like speed, lag and crashes.   

## 4.4           Risk analysis
|**#**|**Risk** |<p>**Consequence**</p><p>**(1-3)**</p>|<p>**Probability**</p><p>**(1-3)**</p>|<p>**Overal Risk**</p><p>**(CxP)**</p>|<span style="color:green">**Low,</span><p style="color:orange">  Medium or**</p><span style="color:red"> **High** </span>| **Action to minimise the risk**|
| :- | :- | :- | :- | :- | :- | :- |
|R1|Dropout of the subject|<p></p><p>3</p>|<p></p><p>1</p>|<p></p><p>3</p><p></p>|<p></p><span style="color:green">Low</p>|<p>Split the task the missing person had. inform Ali. talk to the person</p>|
|R2|Lack of motivation|<p></p><p>2</p>|<p></p><p>2</p>|<p></p><p>4</p><p></p>|<p></p><p style="color:orange">Medium</p>| Do more social stuff. Have task you manage to do and say if the task is to hard. Work together. Positive feedback.|
|R3|miscommunication|<p></p><p>2</p>|<p></p><p>2</p>|<p></p><p>4</p><p></p>|<p></p><p style="color:orange">Medium</p>|Good communication. Assign tasks at issue board/git. Don't assume other is doing the task. Check weekly status report.|
|R4|Internal conflict|<p></p><p>2</p><p></p>|<p></p><p>1</p>|<p></p><p>2</p>|<p></p><span style="color:green">Low</p>|Communicate if problems. Be open for feedback.|
|R5|Wrong focus|<p></p><p>3</p><p></p>|<p></p><p>2</p>|<p></p><p>6</p>|<p></p><span style="color:red">High</p>|Ask TA if the group are not sure what to do and get us in the right path. Discuss tasks.|
|R6|Loss of files|<p></p><p>3</p><p></p>|<p></p><p>2</p>|<p></p><p>6</p>|<p></p><span style="color:red">High</p>|Backups (to OneDrive or other hard-disk),commit and push to git. Organize your file structure so its easy to find your files|
|R7|Misunderstanding of the task|<p></p><p>3</p><p></p>|<p></p><p>2</p>|<p></p><p>6</p>|<p></p><span style="color:red">High</p>||
|R8|Failed to stay aware of the time|<p></p><p>3</p><p></p>|<p></p><p>3</p>|<p></p><p>9</p>|<p></p><span style="color:red">High</p>|Plan your time. Prepare for meeting both with team and TA. Give task so people know what to do. Inform other if tou don't have time to finish your task. Control as a group the work that is not complete|
|R9|Don't attend meetings|<p></p><p>2</p><p></p>|<p></p><p>1</p>|<p></p><p>2</p>|<p></p><span style="color:green">Low</p>|Read the meeting report. Give message if you cant attend|
|R10|Sick so you don't get work done for a week|<p></p><p>2</p>|<p></p><p>2</p>|<p></p><p>4</p>|<p></p><p style="color:orange">Medium</p>|Informe that you are sick. Ask for a more easy task if you are not 100%.|


 **Risk analyse** | Smal Consequence | Medium Consequence|High Consequence|
|---|---|---|---|  
|**verry possible**| | |<span style="color:red">R8</p> | 
|**possible**| |<p style="color:orange">R2/R3/R10</p>|<span style="color:red">R5/R6/R7</p> |     
|**less possible**| |<span style="color:green">R4/R9|<span style="color:green"> R1</p> |

 ## 4.5 Quantifiavle and non-quantifiable benefits
 Impact goals:
 - Easy and faster to manage a mini golf tournament.
 -  Reduce paper work
 - Manage more with the same staff

 Quantifiable benefits:
 - Reduces cost and time of employes because the work get done faster and need less people
 
 Non-quantifiable benefits: 
 - Less stress and easy for the employe to manage a tournament
     

## 4.6           Estimated costs

The estimated cost wil be around 800.000 kroner cause of the hourly rate and 5 people working on it.

<br>
<br>
<br>

# 5.             Product Features

The following is a list and brief description of each of EasyPut's main features. The overarching functionality revolves around the management of mini-golf tournaments, as well as the setup of a tournament for later use through file management. For priority of implementation, see section 9, Precedence and Priority.

<br>

## 5.1 CLI Interface
The program features a lightweight command line interface, allowing the user to interact with the program through input of text, button presses and interaction with a rudamentary menu system.
<br>
<br>
<br>

## 5.2 Storing And Loading Data From File
The ability to store and load tournament and player data to and from a file. The feature includes both completed and active tournaments, as well as the ability to load a pre set-up, ready-to-play tournament from file, or saving a tournament setup
for later use.
 <br>
 <br>
 <br>
## 5.3 Set-up of a New Tournament
Setting up a new tournament for play, including registration of players, teams, choice of scoring-format and team-play vs induvidual players, and what tracks the tournament is to be played on. 

### 5.3.1 Registration of Players 
Registration of players for a tournament, storing what team(if any) they are on, player name and handicap. 

### 5.3.2 Registration of Teams
Registration of teams for a tournament with team name and info about what players are playing for the team.

### 5.3.3 Choosing Holes to be played
Selecting holes from an existing roster of holes to be played, as well as being able to change the order they are played in.

### 5.3.4 Seeding of Players
Randomization of the order the players will play in. Can be performed repeatedly. Player order is independent of teams.

### 5.3.5 Editing Player or Team
Editing already registered data about a player or a team. 

### 5.3.6 Finalizing Registration
Finishing the setup of a tournament and saving it to memory or file. After finalization, details about players and teams cannot be changed.
<br>
<br>
<br>

## 5.4 Setting a tournament as active
Loading a ready to play tournament from file then activating it, or setting registering one, finalizing it and then setting it as active. An active tournament is one currently in play.
<br>
<br>
<br>

## 5.5 Management of an Active Tournament 
Updating player score/stroke during an active tournament, either by automatically going through the list of active players and inputting score for each one, or by selecting a particular player and/or a track for update. Display results, players or tracks.

### 5.5.1 Displaying Current Results by Player
Displaying the current result for a single player, including strokes per hole, total score so far, and handicap.

### 5.5.2 Displaying Current Total Result for all Players
Displaying total result for all players so far in the tournament.

### 5.5.3 Displaying Results for All Players for a Chosen Track. 
Displaying the result of all players for a track chosen by the tournament administrator. 

### 5.5.4 Registration of Results for All Players - In Order
Registration of strokes for players up to a maximum of ten strokes. The program will automatically jump to the current hole being played and the first player that has yet to have a score registered for that hole. 

### 5.5.5 Registration of Results by Track and Player
Registration of strokes for a selected player on a selected track up to a maximum of ten strokes, with the ability to overwrite an earlier registered result if desired.

### 5.5.6 Setting a Tournament as Finished
Ending a tournament and saving the results to file. 
<br>
<br>
<br>

## 5.6 Loading a Finished Tournament From File
Loading a finished tournament from file, enabling much the same features
as managing an active tournament, but disabling the features allowing the registration of scores, effectively letting the user see old results through the display result features.
<br>
<br>
<br>

## 5.7 "Stroke Play" Scoring Format
Total strokes per player is recorded for each track. At the end, the player/team with lowest total is the winner.
<br>
<br>
<br>

## 5.8 "Stableford" Scoring Format
Scoring format with par and handicap, score being adjusted from the players handicap. Player or team with lowest total wins.
<br>
<br>
<br>

## 5.9 "Match Play" Scoring Format
Players or teams are awarded a point for each hole they've bested their competitors. At the end, the player or team with the most points win.
<br>
<br>
<br>

# 6.             Quality Ranges

Define the quality ranges for performance, robustness, fault tolerance, usability, and similar characteristics that are not captured in the Feature Set.

 The program have a optimal performance that we can get out of an CLI. The program is going to be robust so you cant do any huge mistakes and minimal with bugs. The usability is gonna be as very good after all the user testing we are going to do. 


# 7.          Documentation Requirements

This section describes the documentation that must be developed to support successful application deployment.

## 7.1         User Manual

Describe the purpose and contents of the User Manual. Discuss desired length, level of detail, need for index, glossary of terms, tutorial versus reference manual strategy, and so on. Formatting and printing constraints must also be identified.

## 7.2         Read Me File

A document that includes installation instructions and configuration guidelines is important to a full solution offering. Also, a Read Me file is typically included as a standard component. The Read Me file can include a &quot;What&#39;s New With This Release&quot; section, and a discussion of compatibility issues with earlier releases. Most users also appreciate documentation defining any known bugs and workarounds in the Read Me file.

## 7.3 Use Case model and textual description

The Use-Case model shows how the program was planned to be viewed from the users perspective, in regards to what functions the user can use to interact with the program. Our use-case model was updated throughout the process of creating the prototypes with regards to how we wanted to structure the final product, though in the coding part of this assignment some interactions deviated from the diagram to fit the deadline, some functions were left out of the final product in order to deliver the product on time, these functions were not crucial to the general functionality of the program.


## 7.4 Domain model
The domain model shows all the objects within our domain and how they're associated with each other. in the first iteration, we had the admin object that was not connected to any other object and was also up for debate if it was going to be implemented in the final product. The "admin" object was intended to be an admin-account for more access to the system and its functionality, these functions were intended to not be accessible to the "regular" account so that un-authorized users could not do any major "damage". Since we did not find the need for this object, we decided to remove it in the second iteration of the domain model.



## 7.5 Sequence diagram

since the program is lightweight and the functionalities only pass between the user and the system, the sequence diagrams are rather simple. The thought behind how they are constructed is that the user communicates with the interface, and when necessary, the interface communicates with the rest of the system. What is meant by this is that the user does not need to know what data is already in the system and therefore cannot be added again as a duplicate, when the interface gets an input from the user, it automatically checks in its loaded data if the data from the input is a duplicate or not, that part of the communication is handled without the user's involvement, because of that we decided to make the &quot;Interface&quot; and the &quot;System&quot; two separate entities in these models.

## 7.6 Usability

To make the program as intuitive to use as possible, we have focused a lot of the tasks and questions in the user-test's around how intuitive it is for the users to maneuver throughout the program and to what extent the program is memorable and satisfactory. Since the program is not designed for a specific type of user, we decided to perform user-test's on people from different age-groups and with a ranging proficiency with computers to get a picture on how different demographics would experience working with our program.

## 7.7 Universal Design

Given that the program is developed to run in the windows terminal, there are limited possibilities for changing the design and composition of the user-interface, We added breadcrumbs to the top of the screen and made the contrast between the text and the background to be softer than the interacting part, as to show the user where they are in the program, and at the same time, not overpower the rest of the text on the screen. For the rest of the program, we decided to keep the standard black/white theme of the windows CLI since it is high contrast and easily readable, even for people with colorblindness.

## 7.8 User tests

For every iteration of the prototype we did a user-test to get feedback for the improvements we could implement in the next iteration, the user-test were designed to challenge the user, in order to test the usability and the robustness of the system.
<br>
In the first user-test we focused on the functionality and layout of the menu and how intuitive it is for a new user to maneuver, the second user-test also had the focus on functionality, but also had the users try to correct any "mis-input" they might have entered.
The feedback gathered from the User-test were also used to improve further user-tests, when evaluating the completed user-tests we discussed what we needed more feedback on, or what feedback had to be more in depth.


# 8. Precedence and Priority

In this section we describe the priority of system features and functionality as described in section 5. Each* has been assigned a letter M, S or C for "Must have", "Should have" and "Could have", indicating the general importance of a feature to the finished product in accordance with the customers needs.

In addition to lettering, each is assigned a number 1 through 3, indicating the relative priority of a feature within the grouping it belongs to. A feature with a rating of M3 takes precedence over a feature with the rating M2.

<br>
*Sub-features are only included if their rating deviates from their parents rating.

---

| ID | Description | Priority Rating | Comment |
|---|---|---|---|
| 5.1 | CLI Interface | M3 | Indispensible core feature |
| 5.2 | Save/Load to/from File | M3 |  Core feature. |
| 5.3 | Set-up of Tournament | M3 | Indispensible core feature. |
| 5.3.2| Registration of Teams | S3 | Customer can live without team play as its not commonly requested at their facility. |
| 5.3.4 | Seeding of Players | C3 | Not critical to overall functionality | 
| 5.4 | Setting Tournament as Active | M3 | Important to differentiate a tournament already played or ready to play from one being played. |
| 5.5 | Management of an Active Tournament | M3 | Indispensable core feature. |
| 5.6 | Loading/Viewing Finished Tournament From File | S3 | Not critical to the programs main purpose, but a desired feature. |
| 5.7 | "Stroke Play" Scoring Format | M3 | Indispensible core feature
| 5.8 | "Stableford" Scoring Format | C3 | Nice to have extra feature if time permits
| 5.9 | "Match Play" Scoring Format | C3 | Nice to have extra feature if time permits

# 9. A Feature Attributes

This section will discuss the feature attributes proposed for the program.

## 9.1 A.1 Satus
| | |
|-|-|
|**Proposed** |Add teams to tournaments|
|**Approved** |make new tournament, edit tournament and manage holes|
|**Incorporated** |- | 
## 9.2 A.2 Effort
Make and edit tournament and manage holes needed most time because it is the base of the program. The hole program is based on does functions. 
## 9.3 A.3 Stability
Functions have a high probability to change because og all the testing we are going to do. The user comes with feedback. 
And we are going to adjust the program after the feedback. But we have planned so good what the program should do, with a wireframe, sequence diagram, class diagram and domain model. This diagrams makes shows us what to do and what we need to have in our program. It is only after user-test maybe we have to change a bit.
## 9.4 A.4 Target Release
The first version of the program is the MVP. MVP has only features we need to have in the program to tets it on users. 
## 9.5 A.5 Assigned To
We have one who is responsible for the code and what people should do. 
## 9.6 A.6 Reason
The reason why we don't have made it possible to make team is because the client didn't say we needed to make it. So we concluded that we didn't have time and resources to add it.

## Other functions

**Module/function (command):** Open tournament ('O')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | There are saved files available | Inputs number of a file | XX | A confirmation as to whether or not to open (Y/N) | The confirmation is displayed, and the file is opened and ready to manipulate | OK | |
| 2 | Wants to exit the menu | Pressing button | '0' | You will be returned to the main menu | You are returned to the main menu | OK | |

---

**Module/function (command):** Set tournament as active ('A')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | A tournament has been created or loaded | Pressing button | 'A' | A confirmation as to whether or not to set current tournament as active | The confirmation is displayed aswell as a message stating that 'your tournament is currently not active'. There is also two reminders displayed that says 'an active tournament cannot be edited' and 'to register strokes the tournament has to be active' | OK | |
| 2 | Point 1 is finished | Pressing button | 'Y' | A prompt to press 'enter' to proceed | The tournament has been set to active, and can now be played | OK | |
| 3 | Point 1 is finished | Pressing button | 'N' | Return to main menu | You are returned to the main menu, and the tournament has not been set to active | OK | |

---


## Managing an active tournament

**Module/function (command):** Update strokes by current hole ('U' 'C')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | Wants to exit the menu | Exiting the menu | '0' | A prompt to press 'enter' for confirmation | The prompt is displayed, and you are returned to the previous menu | OK | |
| 2 | The input is not an integer, or is outside the legal range | Inputs character | XX | An error message | 'Not a valid stroke value' is displayed, and you can try again or exit | OK | |
| 3 | An acceptable integer is added | Inputs number | '1-10' | Process will be repeated until all players have a score for the first hole | The process is repeated until all players have a score, or you type '0'(exit) | OK | |

---

**Module/function (command):** Display score per player ('D' 'P')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | There are players and holes to be displayed | Pressing button | 'P' | A table is displayed, showing players, holes, score per hole and total score | The table is displayed | OK | |

---

**Module/function (command):** Display participants and holes ('R')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | There are players and holes to be displayed | Pressing button | 'R' | The registered players and holes will be displayed | The players and holes are displayed | OK | |

---

**Module/function (command):** Finalize and end tournament ('F')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | Tournament lacks strokes for some, or all players | Finalizing the tournament | 'F' | Message regarding the lack of strokes | The message is displayed, aswell as a reminder that a finalized tournament no longer can be edited | OK | |
| 2 | Tournament has registered score for all players on all holes | Finalizing the tournament | 'F' | Confirmation prompt (Y/N) | An error message was displayed, although there was no problem actually finalizing. The message regarding the lack of strokes still appeared. | Not OK | The problem was since identified, and the status is now OK. The problem was a faulty loop |

---


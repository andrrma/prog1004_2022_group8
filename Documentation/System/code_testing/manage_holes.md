## Manage holes
**Module/function (command):** Show holes ('S')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | There are holes available in the file | Pressing button | 'S' | A list with all holes, their number, and the par | The list is displayed with all relevant data, aswell as a prompt that returns you to the previous menu | OK | |
| 2 | There are no holes available | Pressing button | 'S' | No holes will be displayed | No holes are displayed, and you can press 'enter' to return to the previous menu | OK | |


---

**Module/function (command):** Finalize new hole ('N' 'F')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | A name and a par is added | Pressing button | 'F' | A confirmation as to whether or not to save the new hole (Y/N) | The confirmation prompt is displayed | OK | |
| 2 | Point 1 is finished | Pressing button | 'Y' | Hole will be saved | The hole is saved and can be used in the tournament. Press 'enter' to return to the previous menu | OK | |
| 3 | Point 1 is finished | Pressing button | 'N' | The hole will not be saved | The hole is not saved. Press 'enter' to return to the previous menu | OK | |
| 4 | A name and/or a par has not been added | Pressing button | 'F' | A message that states that the hole is not complete | The message is displayed. You cannot store an unfinished hole. press 'enter' to try again | OK | |


---

**Module/function (command):** Delete hole ('D')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | There are available holes in the file and the input is inside the range of holes | Inputs integer | XX | A confirmation as to whether or not to delete hole | The confirmation prompt is displayed | OK | |
| 2 | Point 1 is finished | Pressing button | 'Y' | Hole will be deleted | The hole is deleted. Press 'enter' to return | OK | |
| 3 | Point 1 is finished | Pressing button | 'N' | Hole will not be deleted | The hole remains in the list and is still available. Press 'enter' to return | OK | |
| 4 | Wants to exit the menu | Exiting the menu | '0' | A prompt for exiting | Exited the delete-hole-menu. Press 'enter' to return | OK | |

---
## Setting up a new tournament

**Module/function (command):** Player registration ('P')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | Name contains only letters and/or space inbetween | Inputs name | xxx | name will show as confirmed | Name is confirmed | OK | |
| 2 | Name contains more than 25 characters | Inputs name | xxx | name will not show as confirmed | no confirmation | OK | |
| 3 | Name contains norwegian letters | Inputs name | xxx | Name will not be accepted | No confirmation | OK | |
| 4 | Name is accepted and confirmed | Saves the name | 'S' | Name will be added, with an assigned player number | Confirmed and player number assigned | OK | |

---

**Module/function (command):** Choose holes to be played ('H')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | Wants to exit the menu | Exits the hole-menu | '0' | A prompt to press enter for confirmation | A prompt for confirmation appears | OK | |
| 2 | The hole number assigned is outside the range | Inputs number | xx | An error message | 'Error: integer out of range' | OK | |
| 3 | The input is not an integer | Inputs character | xxx | An error message | 'Error: not an integer' | OK | |
| 4 | An acceptable integer, from the given list, is added | Inputs number | '1-31' | A confirmation of the chosen hole | A subsequential confirmation prompt is displayed: 'Y/N' | OK | |
| 5 | Point 4 is finished | Confirms the chosen hole | 'Y' | Hole will be added | The hole is confirmed and added. 'enter' to continue | OK | |
| 6 | Point 4 is finished | Denies the chosen hole | 'N' | The hole will not be added | The hole is not saved. 'enter' to continue | OK | |

---

**Module/function (command):** Edit registered player ('E' 'P')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | No player data is registered | Inputs integer other than '0' | xx | An error message | Error message displayed. You are returned to the choosing of what to edit | OK | |
| 2 | Wants to exit the edit-menu | Exits the menu | '0' | A prompt to press enter for confirmation | A prompt for confirmation appears | OK | |
| 3 | Regitered player is chosen and accepted | Inputs new command in the following menu | 'N' | A name prompt | Name prompt is displayed, and name can be edited | OK | |
| 4 | Registered player is chosen and accepted | Inputs new command in the following menu | 'D' | A confirmation of the player which is to be deleted | A subsequential confirmation prompt is displayed: 'Y/N' | OK | |
| 5 | Point 4 is finished | Confirms that the player should be deleted | 'Y' | Player will be deleted | The player is deleted, and you are returned to the previous menu | OK | |
| 6 | Point 4 is finished | Denies that the player should be deleted | 'N' | Player will not be deleted | The player is not deleted, and you are returned to the edit-player-menu | OK | |

---

**Module/function (command):** Edit registered hole ('E' 'H')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | No hole data is registered | Inputs integer other than '0' | xx | An error message | Error message displayed. You are returned to the choosing of what to edit | OK | |
| 2 | Wants to exit the edit-menu | Exits the menu | '0' | A prompt to press enter for confirmation | A prompt for confirmation appears | OK | |
| 3 | Regitered hole is chosen and accepted | Inputs new command in the following menu | 'N' | A name prompt | Name prompt is displayed, and name can be edited | OK | |
| 4 | Registered hole is chosen and accepted | Inputs new command in the following menu | 'D' | A confirmation of the hole which is to be deleted | A subsequential confirmation prompt is displayed: 'Y/N' | OK | |
| 5 | Point 4 is finished | Confirms that the hole should be deleted | 'Y' | Hole will be deleted | The hole is deleted, and you are returned to the previous menu | OK | |
| 6 | Point 4 is finished | Denies that the hole should be deleted | 'N' | Hole will not be deleted | The hole is not deleted, and you are returned to the edit-hole-menu | OK | |

---

**Module/function (command):** Display holes and players ('D')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | There are players and holes to be displayed | Pressing button | 'D' | The registered players and holes will be displayed | The players and holes are displayed | OK | |
| 2 | There are no players and/or no holes | Pressing button | 'D' | No players or holes will be displayed | Only 'Registered Holes' and 'Registered players' were displayed, but no other data | OK | |

---

**Module/function (command):** Save the tournament ('S')


| **Nr.** | **Condition** | **Action** | **Input** | **Output** | **Achieved result** | **Status** | **Comment** |
|---------|---------------|------------|-----------|------------|---------------------|------------|-------------|
| 1 | At least 2 players and 1 hole is registered | Saves the tournament | 'S' | A confirmation that the tournament is saved | The confirmation is displayed and the tournament can be played | OK | |
| 2 | Too few players or holes registered | Saves the tournament | 'S' | A confirmation prompt that asks if it should be saved, regardless of it being unplayable | The prompt is displayed  | OK | |
| 3 | Point 2 is finished | Confirms that the tournament should be saved | 'Y' | The tournament will be saved | The tournament is saved, but cannot be played | OK | |
| 4 | point 2 is finished | Denies that the tournament should be saved | 'N' | The tournament will not be saved | The tournament is not saved, and you can still edit it | OK | |
| 5 | There is already an open tournament file, when saving the new tournament | Saves the tournament | 'S' | The old, open, file should remain untouched, while the new tournament should appear as a new file | Instead of making two separate files, the old file is overwritten by the new | Not OK | Will be fixed if we have got the time |

---
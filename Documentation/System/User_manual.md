# EasyPut v1.0 User Manual

[[_TOC_]]

---

## 1. Getting started

---

<br>

### 1.1 Starting the application

The first step in getting started with managing a tournament is to simply boot up the program by double clicking the .exe file located in your EasyPut installation folder. Once the program is running you'll be presented with a menu giving you the options:

<br>

N - New Tournament\
O - Open Tournament\
M - Manage Holes\
Q - Quit

<br>

From here you'll be able to create a new tournament from the ground up, open a tournament from a save-file, add/remove mini-golf holes from your save-file for later and current use, and terminate the application.

NB! Make note that EasyPut v1.0 is only provided for Windows operative systems, and is only guaranteed to work with Windows 10 32- and 64-bit systems. Support is not provided for earlier versions at this time.

<br>

### 1.2 Knowing where you are

At any time you will be able to tell how deep you are in the menu system by paying attention to the breadcrumbs displayed in gray lettering along the top of the program. The main menu will for instance be displayed as "/main_menu" indicating that you are in the main menu.

Stepping into the New Tournament menu will result in a breadcrumb text "/main_menu/new_tournament", letting you know you are in the the New Tournament menu which is a sub menu of the Main Menu.

<br>

### 1.3 Traversing Menus

As EasyPut is a simple CLI style program, all menu interaction happens through the input of text. When faced with a menu, you are expected to input the letter associated with the menu item you intend to use. For instance, if you get the option "S - Save" in a menu, inputting the letter 's' and pressing enter gives you the option to save. The program is built to handle the input of longer strings, and it will take the first letter in the string and decipher that as your input.

In other cases you might be expected to input a number to indicate a selection of a player, hole, or save file from a list. In these instances 0 will behave as your cancel/return option.

<br>

Inputting the wrong type of data is generally safe as EasyPut validates input before making use of it, but in general we recommend you input expected data to avoid unforeseen behavior while traversing the menu, which will always be single letters or numbers unless prompted for a name. Any unneeded whitespace will also be removed from your input before it is deciphered by the program.

<br>

### 1.4 Terminating the application

You can close down the application at any time by clicking the X symbol in the top right corner, but for the sake of save-file integrity we strongly recommend you use the Main Menu feature "Q - Quit" to close the application as you'll be presented with the option to save your current tournament if one is loaded to the program memory.

<br>

### 1.5 Information on Names

As a slight limitation of the program, special alphabetical symbols such as æ,
numbers and similar non alphabetical characters are not allowed in player and hole names. Tournaments can however contain special symbols. For the sake of display integrity, name lengths are limited to 25 characters in length.

<br>
<br>

---

## 2. Creating, Editing and Loading Tournaments

---

<br>

### 2.1 Creating a New Tournament

After following the steps in section 1, you can now input the letter 'n' followed by pressing enter, and you should find yourself in the New Tournament Menu:

<br>

N - Tournament Name:\
p - Add Player\
H - Add Holes\
E - Edit Player/Hole\
D - Display Holes and Players\
S - Save\
B - Back to main menu

<br>

At this point you can start by naming your tournament, adding players or holes.

<br>

### 2.1.2 Naming Your Tournament

The option 'N - Tournament Name:' will let you input a name. Note that non-alphabetic symbols are not allowed. Once you have written the name, up to a total of 25 letters in length, you only have to press enter and it will be updated. The name not showing up after inputting a string indicates it contained illegal symbols or that it was too long.

<br>

When a name has been set, the menu option will change to 'N - Edit Name:'.
Repeating the earlier step will overwrite the old name, letting you change it at any time during the setup.

<br>

### 2.1.3 Adding a Player

The option 'P - Add player' will bring you to the Add Player Menu, where the option 'N - Name:' will let you input a player name with a max of 25 characters. No symbols are allowed in player names. Once a name is registered, you can use the option 'S - Save' which will save the player to your tournament and return to the New Tournament menu, use the 'N - Edit Name:' option to change the name, or use 'B - Back' to return without saving the player.

<br>

### 2.1.4 Adding a Hole

The option 'H - Add Hole' will present you with a list of all holes available, letting you choose one by inputting its number. Upon inputting a valid number you will be asked to verify. Verifying will save it to the tournament, otherwise you will simply be given the option to select another one or to exit by inputting '0'.

<br>

### 2.1.5 Editing Player or Hole

The option 'E - Edit Player/hole' will first ask you to choose Player with 'P' or Hole with 'H', or 'C' to cancel. Upon making your selection you will be asked to choose a player or hole with their respective number. Selecting a hole will then let you edit a player/hole's name, delete the player/hole altogether. Regardless of wether you do nothing or change the name, any change is saved automatically, and you can go back with 'B - Back' when done.

<br>

### 2.1.7 Displaying Registered Players and Holes

The option 'D - Display Holes and Players' will print all registered players and holes saved to the tournament to your console screen, giving you an overview of all registries. To return all you have to do is press Enter.

<br>

### 2.1.8 Saving Your Tournament

You can at any time save the Tournament as is it by using the option 'S - Save'. This will save your tournament with it's current name as it's filename in the 'savedata' folder, and you can resume editing the Tournament at a later time by opening it in the 'Open Tournament' menu and then selecting 'E - Edit Tournament' in the Main Menu.

NB! Changes done after a save will not be saved to the savefile automatically. Make sure you do a save before returning to the Main Menu if you've made any changes you'd like to keep.

NB! A Tournament has to have at least -two- players and at least -one- hole to be a valid Tournament for play. You can save a tournament in this state, but it cannot be played until enough players or holes are added.

<br>

### 2.1.9 Discarding or Deleting Tournament

If you have yet to save your Tournament you can discard it at any time by using the option 'B - Back' from the menu. If a file is already saved, however, you'll have to manually delete it in the 'savedata' folder if you want to be rid of it. This folder can be found in your EasyPut installation directory together with the .exe file.

<br>

### 2.2 Editing Tournament

Provided the tournament has yet to be set as an active tournament, meaning it hasn't been set ready for play, you can at any time edit it by choosing the option 'E - Edit Tournament' from the Main Menu. Note that this option will -only- be on the menu if a tournament is loaded to memory and it is eligible to be edited, meaning it isn't an already finished tournament or one that is still actively being played. 

<br>

The Edit Tournament menu is a essentially the same as the New Tournament menu, and behaves in the exact same manner, only that it will edit a tournament loaded to memory rather than creating a new one. See section 2.1 - 2.1.9 for specifics.

<br>

### 2.3 Opening a Tournament From File

The 'O - Open Tournament' option in the Main Menu will present you with a print out of all save files found in the 'savedata' folder before asking you to select one with it's number, or '0' to cancel. You can open all tournament files, but you will have differing options present in the main menu depending on whether the tournament is playable, already played, or active.

- An already played Tournament can only have it's results viewed, not be edited or have strokes registered.
- An active Tournament can't be edited, but you can register strokes for players.
- A non playable (less than two players or no holes) Tournament can only be edited.

<br>
<br>

---

## 3. Managing a Tournament

---

<br>

### 3.1 Setting a Tournament as Active

If you have a Tournament loaded to memory that has at least two players and one hole that has yet to be set as active, the Main Menu will present you with the option 'A - Activate Tournament'. Upon using this option you will be given the choice to activate the tournament, meaning it will be flagged as an actively played tournament, meaning you can start registering strokes for players, but it can no longer be edited.

<br>

NB! Make absolutely sure your Tournament is set up correctly before you set it as active as there is no way to undo this, and it can no longer be edited after activation.

<br>

### 3.2 Managing an Active Tournament

If the Tournament loaded to memory is already set as active, the Main Menu option is instead called 'A - Manage Active Tournament'. Using this option will present you with the following sub menu:

<br>

Active Tournament:\
U - Update Stroke\
D - Display Score\
R - Display Participants and Holes\
S - Save Tournament\
F - Finalize and end tournament\
B - Back to main menu

<br>

### 3.2.1 Updating Stroke

The 'U - Update Stroke' option will lead you to a sub menu where you can select either 'C - Current Hole' or 'T - Select a Hole'.

Selecting 'C - Current Hole' will always automatically ask you to input a stroke for the first player on the first hole that has yet to have a stroke registered, letting you go through all holes and players in order. Once a Hole has a result registered for all players, it will inform you that a stroke has been registered for all players, and you'll be taken back to the 'U - Update Stroke' menu. Next time you select 'C - Current Hole' the program will automatically jump to the next Hole in need of having strokes registered, or it will display the last hole if all holes are done.

<br>

Strokes are capped at a maximum of ten, so the program only accepts a stroke from 1 through 10. You can also cancel input at any time by inputting '0'. 'B - Back' lets you return from the Update Stroke menu to the Active Tournament menu.

<br>

### 3.2.2 Display Score

The 'D - Display Score' option will display the score of every player for every hole as stroke under par. A score of -1 indicates the player managed to finish with a stroke one less than the par of the hole. If a stroke is yet to be registered for a hole, the score for that hole is displayed as an 'x'.

<br>

### 3.2.3 Display Participants and Holes

The 'R - Display Participants and Holes' option will print out a list of all players participating in the tournament, as well as all golf holes being played.

<br>

### 3.2.4 Save Tournament

The 'S - Save Tournament' option will save the tournament in it's current state to it's file in the 'savedata' folder. As there is no automatic save feature, we recommend saving often.

<br>

### 3.2.5 Finalizing Tournament

The 'F - Finalize and End Tournament' option will let you set the tournament as complete regardless of whether it has a stroke registered for all players on all holes or not, but be warned that once set as finalized, you will no longer be able to update strokes.

<br>

### 3.2.6 Returning to the main menu

The 'B - Back to main menu' will take you back to the main menu. Take note that while the strokes registered will not disappear, they won't be saved unless you save the tournament to file.

<br>

### 3.3 Viewing Old Results

If the Tournament currently loaded to memory has been finalized, the Main Menu option will instead be named 'A - View Old Tournament'. This will allow you to use the Display Score and the Display Participants and Holes features, but not saving or updating the stroke for any players.

<br>

---

## 4. Managing Holes

---

Using the option 'M - Manage Holes' from the Main Menu gives you access to the following sub menu:

<br>

Manage Holes:\
S - Show holes\
N - New Hole\
D - Delete Holes\
B - Back to main menu

Using this menu you can delete or add holes to the file containing the tracks available to you when creating a tournament. The holes contained in this file should reflect the holes available at your facility, letting you easily add them to a tournament when needed.

<br>

### 4.1 Displaying Stored Holes

Using the option 'S - Show Holes' all holes from the save file will be printed to screen with their number and par. To return to the previous menu, simply press Enter.

<br>

### 4.1.2 Creating a New Hole

Using the 'N - New Hole' option will present you with a new menu:

New Hole:\
N - Name:\
N - Par:\
F - Finalize and Save\
B - Back to manage holes

The 'N - Name' option will prompt you for a name, while the 'P - Par' option will prompt you for a par from 1 to 10. Either can be repeated to overwrite the old input.

Once both a name and par has been set, you can save the hole with 'F - Finalize and Save'. The hole will be added to the holes_save_data.dta file in the 'savedata' folder.

To abort the creation of the new hole you can simply use 'B - Back to manage holes' without saving the hole.

<br>

### 4.1.3 Deleting a Hole

Using the option 'D - Delete Hole' you'll be presented with a printout of all currently stored holes and a prompt to either input the number of the hold you'd like to delete, or '0' to cancel. Inputting a valid number will give you a prompt for confirmation before deleting the hole. Upon confirmation the corresponding hole will be deleted.

NB! There is no way to undo a deletion. Make absolutely sure you want the hole gone before performing a deletion.

<br>


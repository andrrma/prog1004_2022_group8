
[[_TOC_]]


# Repository

Contains source code files, internal team documentation and documentation that is otherwise mirrored on the wiki.

<br>

## Documentation

---

All working copies of files used for our wiki are kept and maintained within this folder in markdown format, as well as UML models in .png format, along with internal organization documentation.

### Organization

Contains data concerned with internal team organization, weekly reports, hours, gantt diagram, and meeting data.

#### Meeting summons

Summons per planned meeting with agenda, location, date and time.

#### Meeting minutes

Minutes from meetings with details per point on the meeting agenda. Should contain enough information that someone not present can follow decision making and discussions.

#### Weekly reports

Weekly progress reports from each team member. One markdown file per team member containing reports for all weeks of project duration.

#### Collaboration agreement

Collaboration agreement between team members, functioning the basis of internal decision making and expectations and responsibilities between members of the group during the duration of the project.

### Requirements

#### Usability testing

Anonymized test data from the first and second prototype iterations, test template and a report to go with each test detailing findings and solutions to problems found during the test.

#### Other documentation

Contains working copies of diagrams and text mirrored on the wiki regarding product requirements.

### System

#### Code testing

Contains sub folders detailing the tests of the program features as they are implemented for the last product iteration of the project duration.

#### Other Documentation

Contains working copies of files concerning the project system mirrored on the wiki.

### Main Report

Main project report.

<br>

## Src

---

### Source .cpp

All C++ source files in use for the project, as well as some that are currently not in use/implemented in the last iteration. Code not in use is commented out, but the files are kept for (theoretical) future implementation during the software lifecycle.

### Include

All header files used within the .cpp files of the source code.

### Save-data

All save files the program itself depends on to function while compiled. Same folder one would find packaged with the compiled application.
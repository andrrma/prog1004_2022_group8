# Installation of EasyPut v1.0

EasyPut is provided as a zipped folder containing the application, its save directory and the user manual. Installing the program is a simple matter of unzipping the folder in a directory where one has user privileges. From there on one can run the application right away and it will function as intended, provided the directory has appropriate privileges for the current user.

Make note that EasyPut is only provided for Windows operative systems, and is not intended for use with Mac OS or Linux distros such as Ubuntu.
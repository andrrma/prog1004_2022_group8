# Persistence

EasyPut v1.0 makes use of simple disk storage for data persistence, both for Tournament states and the available golf holes available to the user when creating a new Tournament.

## Tournaments

Tournaments are saved as plaintext files with a .mgf (mini golf file) suffix in the save-data folder located in the same root folder as the EasyPut application itself. The user can at any given time save the state of the tournament as it is in current memory, provided it has been given a name, allowing them to resume the management of an actively played tournament after unexpected program termination, create a tournament and then save it for later use, or save a finished tournament for later review of scores. 

A tournament file contains data about all participants, holes the tournament is being played on, whether the Tournament is being played, or if it has been played. In addition it keeps track of strokes per player per hole.

## Mini-Golf Holes

Like Tournaments, Holes are also saved as plaintext file in the save-data folder, but with the suffix .dta rather than .mgf to differentiate them and their purpose. While there can be multiple .mgf files, there is only  one file with holes, specifically named 'holes_save_data.dta'. In the case of a missing save-file, the program will attempt to create a new one with whatever holes the user has created that session, if any.
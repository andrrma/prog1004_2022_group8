
Roles and responsibilites are at any given time subject to change if deemed
necessary by the team as a whole due to such reasons as:
- Team member being overburdened
- Demotivating role void of learning or potential for growth
- Drop-out from course
- Sickness
- Swapping roles for the sake of learning
- Role or responsibility being a poor fit for student, or their abilities are better focused elsewhere.

  <br> 
---
---
## Leadership – Project leader: André marthinsen 
Responsibilities:
- Leading meetings in cooperation with meeting responsible.
- Overseeing the project as a whole with progress and milestones.
- Ensuring that team members are given appropriate tasks, as well as ensuring each is given an opportunity to learn, develop and thrive.
- Ensuring team members are given encouragement and opportunity to voice their opinion on work, methods and plans.
- Enabling team work, encourage and maintaining democracy, not dictating or micro-managing.
- Ensuring that the right work is done right, avoiding waste of time and resources.
- Supporting each member in their tasks, helping where needed.


<br>
<br>

## Meeting responsible: Paul Bjørneng 
Responsibilities:
- Managing/Leading meetings in cooperation with project leader.
- Ordering rooms for meetings
- Noting down initial agenda for next meeting at the conclusion of a meeting. 
- Noting down minutes from meetings, but not to such a degree that it inhibits their participation in discussions.
- Coordinating with leader on additional agenda before sending out summons for next meeting. 
- Responsible for finding an appropriate timeslot for meetings. 
- Sending out Meeting summons at least 24 hours in advance. 
- Coordination with the team on how to improve meetings 


<br>
<br>


## Archiving / Documentation 1: Ane 
Responsibilites:
- Researching how to improve efficiency when it comes to documentation, cooperation and the use of Git in coordination with project leader. 
- Documentation structure and archiving, as well as choice of methodology used for maintaining documents. 
- Final quality control of work before hand-in in cooperation with team.
- Ensuring the Wiki is up to date and that information is available to team members now rather than later. 
- Turning in work on Blackboard 
- Coordinating with and supporting Jørgen in their responsibilities.

 
<br>
<br>


## Archiving / Documentation 2: Jørgen 
Responsibilities:
- Distributing and planning work on project documentation in cooperation with team and project leader.
- Ensuring Gantt chart is being used and kept up to date.
- Keeping track of Weekly Reports and hours, ensuring they are being registered.
- Coordinating with and supporting Ane in their responsibilites.

 
<br>
<br>


## Modelling and User testing: BK 
Responsibilities: 
- UML Diagrams and Wireframe for the second iteration. 
- Interaction design decisions in coordination with team. 
- User test development, coordingating user tests with team members. 
- Formulating code standards with project leader.
- Ensuring code standards are followed and code is working at any given time.
- Ensuring models are up to date and cohesive. 
 
<br>
<br>


# Shared Responsibilities.

- Quality check of work before turn in:
- - Should be done by everyone, equally, ahead of each turn in.
- - Everyone is reponsible for turning in quality work.
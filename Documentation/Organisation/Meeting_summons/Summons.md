## Meeting summons




<br>
<br>

# Meeting summon: Project team 8 W11 nr.2
Time/location: 16/3/2022, 14:00 - 16:00: NTNU Campus, Room A266

### The following persons are called for: 

Paul Bjørneng\
Bjørn Kristian Strand\
Ane Malene Olaussen\
Jørgen Teigen

---
## Agenda
Case no. 01/2022: Discussion of potential threats to the project\
Case no. 02/2022: Voting on risks using Risk Poker\
Case no. 03/2022: Review of usability tests from first iteration\
Case no. 04/2022: Review feedback from customer and TA\
Case no. 05/2022: Assess what went well, what could have gone better\
Case no. 06/2022: Discuss how to adress problems uncovered in test and feedback\
Case no. 07/2022: Planning of MVP, backlog for phase 2 and workload distribution


There's quite a few items on our agenda for this meeting, so we might not
get through them all. We're currently in a critical phase, moving on from a cheap
wireframe prototype onto actual code, so planning  properly is of essence to avoid costly mistakes. 

Let me know as early as possibly if you're unable to attend the meeting in person.

-----
Regards\
André Marthinsen\
Gjøvik - 15.03.2022

---
---
<br>
<br>

# Meeting summon: Project team 8 W11 nr. 3
Time/location: 18/3/2022, 14:30 - 17:30: NTNU Campus, Room S409

### The following persons are called for: 

Paul Bjørneng\
Bjørn Kristian Strand\
Ane Malene Olaussen\
Jørgen Teigen

---
## Agenda
Case no. 01/2022: Review feedback from TA meeting\
Case no. 02/2022: Discuss how to adress problems uncovered in test and feedback, continued\
Case no. 03/2022: Apply suggested changes to wireframe prototype\
Case no. 04/2022: Planning of MVP, backlog for phase 2 and workload distribution\
Case no. 05/2022: Assess what went well with the first iteration, what could have gone better




As expected we didn't get through all the points from our last meeting, but we made good progress on some important points. Leftover agenda from last meeting has been carried over.

-----
Regards\
André Marthinsen\
Gjøvik - 17.03.2022

---
---
<br>
<br>

# Meeting summon: Project team 8 W11 nr. 4
Time/location: 22/3/2022, 10:00 - 12:00: NTNU Campus, Atrium/LOGIN

### The following persons are called for: 

André Marthinsen\
Bjørn Kristian Strand\
Ane Malene Olaussen\
Jørgen Teigen

---
## Agenda
**Case no. 01/2022:** Review the 2. iteration of the wireframe

**Case no. 02/2022:** Start the MVP process, assign tasks

**Case no. 03/2022:** Discuss the coming user test of the MVP


-----
Regards,\
Paul Bjørneng\
Gjøvik - 21.03.2022

---
---
<br>
<br>

# Meeting summon: Project team 8 W11 nr. 5

Time/location: 06.04.2022, 12:00 – 14:00, Campus, Atrium

### The following persons are called for: 

André Marthinsen\
Bjørn Kristian Strand\
Ane Malene Olaussen\
Jørgen Teigen

---
## Agenda
**Case no. 01/2022:** Status as of now

**Case no. 02/2022:** Discuss the coming code-work

**Case no. 03/2022:** Plan the writing of the main rapport


-----
Regards,\
Paul Bjørneng\
Gjøvik - 05.04.2022

---
---
<br>
<br>
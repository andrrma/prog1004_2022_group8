# Meeting summon: Project team 8 W11 nr. 5

Time/location: 06.04.2022, 12:00 – 14:00, Campus, Atrium

### The following persons are called for: 

André Marthinsen\
Bjørn Kristian Strand\
Ane Malene Olaussen\
Jørgen Teigen

---
## Agenda
**Case no. 01/2022:** Status as of now

**Case no. 02/2022:** Discuss the coming code-work

**Case no. 03/2022:** Plan the writing of the main rapport


-----
Regards,\
Paul Bjørneng\
Gjøvik - 05.04.2022


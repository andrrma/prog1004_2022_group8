# Minute 
Every minute must be included  
 
## Minute from project meeting in project team 8

Time/location: [\date, time, place]\
Present: <\name>, <\name>…  \
Absent:     No one  \
Moderator: <\name>

**Case no 1/2018** 
lorem ipsum… 
<br>

    **Case no 2/2018** 
    lorem ipsum… 
<br> 

    **Case no nn/2018** 
    Nothing to mention in AOB 

dd.mm.yyyy, <\name> 

---
## Minute from project meeting in project team 8

Time/location: 15.03, 10:00 – 12:00, Campus

Present: André Martinsen, Ane Olaussen, Jørgen Teigen, Bjørn Strand, Paul Bjørneng

Absent:     No one

Moderator: Paul Bjørneng

**Case no. 01/2022:** Assess the current role distribution

- We agreed that the current roles were not necessarily playing to our strengths, and decided to do some official changes, to make sure everyone is familiar with their own responsibilities.

**Case no. 02/2022:** Evaluation of yesterdays client meeting

- We acknowledged the shortcomings of the meeting. As a result, we planned to finish up the risk analysis during the next meeting. We also decided that it would be best if we complete and improve other essential sections of the vision document, before working on the MVP.

**Case no. 03/2022:** Initiate review of feedback from TA

- We analysed some of the feedback, but decided to assess it further in the next meeting.

15.03.2022, Paul Bjørneng

---
## Minute from project meeting in project team 8

Time/location: 16.03, 14:00 – 17:00, Campus

Present: André Martinsen, Ane Olaussen, Jørgen Teigen, Bjørn Strand, Paul Bjørneng

Absent:     No one

Moderator: Paul Bjørneng


**Case no. 01/2022:** Discussion of potential threats to the project

- Potential threats:

- No show for meeting

- Lack of motivation in the group
- Miscommunication
- Internal conflict
- Wrong focus
- Drop out, sickness (1 week absent) , lack of communication
- Loss of files, system crash
- Misunderstanding in regards to division of tasks
- Lack of time 

- We came to an agreement on these potential threats, that could possibly occur


**Case no. 02/2022:** Voting on risks using Risk Poker

- Completed the vote and discussed what to do regarding the potential threats. Finished up the risk analysis for the vision document.


**Case no. 03/2022:** Review of usability tests from first iteration


- Completed the review. Came up with a couple of possible solutions to the different problem statements.

**Case no. 04/2022:** Review feedback from customer and TA

**Case no. 05/2022:** Assess what went well, what could have gone better

**Case no. 06/2022:** Discuss how to adress problems uncovered in test and feedback

**Case no. 07/2022:** Planning of MVP, backlog for phase 2 and workload distribution


Case 4 through 7 we have postponed till Friday due to duration.

16.03.2022, Paul Bjørneng

---

## Minute from project meeting in project team 8

Time/location: 18.03.2022, 14:30 – 18:00, Campus

Present: André Martinsen, Ane Olaussen, Jørgen Teigen, Bjørn Strand, Paul Bjørneng

Absent:     No one

Moderator: Paul Bjørneng

**Case no. 01/2022:** Review feedback from TA meeting

- We have decided to finalize tasks we have begun with, rather than leaving it fragmentary for too long. That, for instance, means we will have to seriously improve the vision document.
- We must also improve on the initial readability, when writing the different documents.
- We will also put the wireframe, and diagrams, as attachments for the next iteration.

**Case no. 02/2022:** Discuss how to address problems uncovered in test and feedback, continued

- Went through the problem statements and solutions. Each team-member came up with some additional, possible, solutions. These we will analyse further, and depending on their importance for the program, apply them.

**Case no. 03/2022:** Apply suggested changes to wireframe prototype

- Went through the wireframe, separately, and discussed the different solutions mentioned in the latter case.
- Considered the changes that could be suitable for the wireframe and later, the program itself.

**Case no. 04/2022:** Planning of MVP, backlog for phase 2 and workload distribution

- Jørgen will start working on the Gantt diagram, as well as the vision document.
- Ane and Jørgen will finish up the risk analysis in the vision document.
- Bjørn begins working on the wireframe for the next iteration.
- André begins with improving on the product features in the vision document.
- The planning of the MVP is postponed until the wireframe is completed on Tuesday.

**Case no. 05/2022:** Assess what went well with the first iteration, what could have gone better

- **What went well:**
- We got started early with preparing and planning the work ahead.
- We effectivly finished much of the different tasks due for the first iteration.
- Good attendance at the team meetings.    


- **Things to improve:**
- The distribution of work load.
- Communication in the group.
- Keeping in mind our different roles and expected assignments, as our previous roles were poorly defined.
- Beware of possible lack in motivation.
- We should keep our meetings more structured and constructive.

18.03.2022, Paul Bjørneng

---

## Minute from project meeting in project team 8

Time/location: 22.03.2022, 10:00 - 12:00, Campus, Atrium

Present: André Martinsen, Ane Olaussen, Jørgen Teigen, Bjørn Strand, Paul Bjørneng

Absent:     No one

Moderator: Paul Bjørneng


**Case no. 01/2022:** Review the 2. iteration of the wireframe

- Analyzed the improved version of the wireframe. Some small changes still need to be done before Friday, otherwise it is looking good. We must also make sure it correlates to the MVP.
- Bjørn will finish the wireframe completely today (Tuesday)

**Case no. 02/2022:** Start the MVP process, assign tasks

- Paul and Jørgen will look into the main score format, &quot;Stroke play&quot;, itself. And also implement the &quot;hole&quot; class.
- Ane will take charge of the CLI interface (the menu set-up).
- André will transfer the logic of the &quot;team&quot; class to the &quot;tournament&quot; class.

**Case no. 03/2022:** Discuss the coming user test of the MVP

- Bjørn will finish the user test set-up today (Tuesday). Aim to have completed the user tests by Thursday.

22.03.2022, Paul Bjørneng

---

## Minute from project meeting in project team 8

Time/location: 06.04.2022, 12:00 – 14:00, Campus, Atrium

Present: Ane Olaussen, André Marthinsen, Bjørn Strand, Jørgen Teigen, Paul Bjørneng

Absent: No one

Moderator: Paul Bjørneng


**Case no. 01/2022:** Status as of now

- There are lots of stuff in our agendas, excluding this project. We will have to manage our time well in the coming weeks.

Vision document:

- Still lots to be done, for instance, introductions to each section, and the user manual. We will have to finish this as quickly as possible.
- Ane and Jørgen will find out what specifically is still to be done, and then put it on the issue board
- Make a milestone for the vision document in the Gantt form.

Other:

- The main report is now the biggest and most important task, and the plan is to start writing it immediately after the vision document is done.
- We must quickly wrap up working on the code, as the documentation now takes priority.
- The project manual has to be done.
- We must change all Wiki files to markdown. Ane takes responsibility for this.

**Case no. 02/2022:** Discuss the coming code-work

- Our code, as of now, is not a long while from being finished.
- André has the main responsibility for finishing the code.

Still to do:

- Access control: low priority, but easily done.
- File management: high priority
- Backup: easily done.
- Tournament class/setup: soon finished.
- Team: low priority, but could be done if enough time remains.
- Seeding players: low priority, maybe exclude.
- Other scoring formats: low priority.

**Case no. 03/2022:** Plan the writing of the main rapport

- We will each go through the rapport template, and also study up on relevant points from the curriculum.

- It is important to actively communicate during the delegation of tasks. But we will all have to take initiative.

06.04.2022, Paul Bjørneng

---

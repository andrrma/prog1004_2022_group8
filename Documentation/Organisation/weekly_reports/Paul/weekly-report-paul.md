# Weekly Reports: Paul Røkke Bjørneng

## Week 1: 
**21.feb - 27.feb**

- Had a few meetings with the team, and also attended an initial meeting with the &quot;customer&quot;, where i mainly acted as the notetaker. 
- Took the role as project leader. 
- Read up on some of the curriculum, to make sure i was prepared for the coming weeks. 
- Downloaded Git, cloned the repository and studied up on how Git is to be used. 

<br>

## Week 2:
**28.feb - 6.mar**

- Did some work on the Gantt chart. 
- Took charge on writing down the product features, aswell as assisting the others with different sections from the vision document. 
- Performed an user test with someone unfamiliar with our program, and noted down the result and their answers to different feedback questions. 

<br>

## Week 3:
**7.mar - 13.mar**

- Not too much happened this week, as the DevOps workshop was ongoing. 
- Did some self study as preparation for the next iteration, including the coming MVP-work. 

<br>

## Week 4:
**14.mar - 20.mar**

- After a meeting where we revisioned how well the initial role distribution had worked, these past weeks, i assumed the role: &quot;Meeting Responsible&quot;. 
- We also reworked the responsibilities of the different roles, as we figured that the more in depth they were, the better we would understand our individual tasks. 
- Read up on how to write in markdown, and the markdown syntax. 
- Wrote the minutes from this week's meetings, booked rooms for the next meetings and made sure they fit with our different schedules. 

<br>

## Week 5:
**21.mar - 27.mar**

- Prepared the agenda for the team meeting on march 22. and noted down the minutes. 
- Wrote some coding regarding the &quot;Hole&quot; class, and made sure the scoring system in our program correlated with the &quot;stroke play&quot; format from the sport of golf. 
- Assisted with other sections of the coding, for instance with the "Tournament" class. 
- Performed another user test on the MVP, and noted down the feedback from the user, using our premade test template. 

<br>

## Week 6:
**28.mar - 3.apr**
- Nothing was done this week.

<br>

## Week 7:
**4.apr - 10.apr**
- Attended the Teams-meeting with the supervisor.
- Had a separate group meeting afterwards, discussing how we should start writing the main report.

<br>

## Week 8:
**11.apr - 17.apr**
- Not too much was done this week on my part.
- Had to read through the curriculum, aswell as the main report template, as to find relevant information for when writing commences.

<br>

## Week 9:
**18.apr - 24.apr**
- Did some self-study and information search regarding the remaining points of the main rapport.

<br>

## Week 10:
**25.apr - 29.apr**
- Performed and documented the final code testing, of all functions included in the "EasyPut" program.
- Wrote the entirety of chapter 4 of the main report.
- Wrote the entirety of chapter 5 of the main report. 
- Wrote chapter 6 of the main report. 




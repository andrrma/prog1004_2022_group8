# Weekly Reports Ane

## Week 1
**21.feb - 27.feb**
- Assigned roles, took on the role to be responsible for archiving and documentation. Main tasks include quality control, ensuring wiki is up to date and turning in work on blackboard.
- Started reading the required documents and began the process of making it our own. 
- Joined the git repo.

## Week 2
**28.feb - 6.mar**
- Worked on the vision document based on agreements made during our meetings.
- Performed usertest of the wireframe.
- Quality checked documents and handed in first iteration.

## Week 3
**7.mar - 13.mar**
- Week consisted of being ill. So I only managed to attend the group meeting.

## Week 4
**14.mar - 20. mar**
- Had a meeting where we went over the roles, kept my initial role and shared the tasks with Jørgen.
- Made a risk analysis along with the other team members.
- Made some changes to the vision document based on feedback from our TA.

## Week 5
**21.mar - 27. mar**
- Programmed menu for MVP.
- Ran quality checks on all diagrams and updated wiki.
- Handed in second iteration.

## Week 6
**28. mar - 3. apr**
- We had a meeting with TA and got some help to continue forward with the program.

## Week 7
**4. apr - 10. apr**
- Busy week with another project 
- Discussed how we wanted to work to be able to finish the project in time.
- Discussed how we wanted the program to look like finished.
- Had a meeting with client(Ali) and got some points on what to work on and what to not work on.

## Week 8
**11. apr - 17. apr**
- Easter break
- Worked on a list on what needed to be done in the vision document in order to finish it
- Read the curriculum to make sure we've followed it and not made any mistakes, as well as getting references for main report
- Wrote the main report as markdown and added points to easily see what needed in the different chapters.
## Week 9 
**18. apr - 24. apr**
- Worked on the vision document and main report
- Read the curriculum

## Week 10
**25. apr - 29. apr**
- Wrote main report and finished up documentation 
- Wrote self reflection
- Prepared myself for the final presentation
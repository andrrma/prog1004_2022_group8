# Weekly Reports: BK

## Week 1:

**21.feb - 27.feb**

- Had a few team meetings to get the administrative part setup, made the project-plan and assigned roles, read up on the parts of the curriculum that i was not entirely bought up on, started to get familiar with the Diagramm.com program for making UML models.

<br>

## Week 2:

**28.feb - 6.mar**

- Did a lot of prototyping with the UML diagram's: Sequence-diagram and Use-case-diagram, and added them to the wiki. had our first meeting with the TA. Completed the first user-test of the wireframe 1. iteration for quality assurance. Had team-meetings regarding the Vision-document and making sure we delivered the correct info for the 1. deliverables.

<br>

## Week 3:

**7.mar - 13.mar**

- Had one team meeting to discuss the status of the project, other than that i spent 3h on learning more about user design and accessability concepts. did not do the most this week since the devops was in progress and we had 2. other assignments/project that took most of the available time this week.

<br>

## Week 4:

**14.mar - 20.mar**

- Had a lot of team-meetings regarding the TA feedback of the first deliverables and revising the roles we assigned at the start of the project, we also had to revise a lot on the approach we took on the documentation part of the project (based on the feedback from the TA).

<br>

## Week 5:

**21.mar - 27.mar**

- Made a lot of changes to the Wireframe, wrote the user test manual for the MVP and made changes to the sequence-diagram to make it up to date. Had problems with getting an IDE to compile and run the program properly, only got it working on friday morning with help from Jørgen so a lot of time went to waste trying to make that work instead of actually coding on the MVP

<br>

## Week 6:

**28.mar - 3.apr**

- did'nt do a whole lot this week as we were waiting on feedback from the TA and took an unplanned "break" in the project. familiarized myself with the feedback from the TA when we got that on the 31. march, looked into how we could add files to the wiki that was not just a download link, settled on just copy/pasting the document over to the wiki page.

<br>

## Week 7:

**4.Apr - 10.Apr**

- 2h team meeting
- did some self learning with the provided lecture notes on blackboard regarding Human computer interaction, especially User Experience and Universal design, in order to analyze the feedback from the user test's we had performed in regard to the MVP.

<br>

## Week 8:

**11.Apr - 17.Apr**

- Spent a lot of time this week trying to make various IDE's function properly with varying success, spent some time in the wiki re-organizing the user-test's as it was requested the user-test's be split into two document 1. Data from the user test 2. Summary that has condensed the user test down to what we're gonna improve in the next iteration.

<br>

## week 9:

**18.Apr - 24.Apr**

- Did some coding on the final product
- went over the various diagrams making sure they were up to date and also without any spelling mistakes.

<br>

## Week 10:

**25.Apr - 1.May**

- Spent a lot of time working on the main rapport, specifically chapters 2 and 3.
- Added the missing parts to chapter 7 in vision document.
- completed a lot of minor tasks regarding the vision document that were listen in the issue-board on git.
- spent a lot of time spell-checking and formatting both documents.
- Did the group evaluation of the project at a team-meeting.
- Wrote self-report for project.
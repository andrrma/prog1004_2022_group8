
# Weekly Reports: Jørgen Teigen

## Week 1: 
**21.feb - 27.feb**

- Did write on point 1 and 2 in vision document. Had some issues on the vision document about what to write solved that by asking other group members. Attended group meeting and first meeting with TA.  
<br>

## Week 2:
**28.feb - 6.mar**

- Write point 1 and 2 in vision document. The user test i had to find a person to test the wierframe on. The test whent pretty well user mange to make his way arond in the wierframe.
<br>

## Week 3:
**7.mar - 13.mar**

- Wrote more on point 1 and 2 in vision document. 
<br>

## Week 4:
**14.mar - 20.mar**

- Wrote on risk analyse and made a tabel for it.  
<br>

## Week 5:
**21.mar - 27.mar**

- Wrote more on point 1 and 2 in vision document. And made a gantdiagram wich i used excel for, had a bit problem with excel and the time function. Did solve that by starting on scratch. Made some function to the MVP. Did also have a usertest for the MVP.

## Week 6: 
**28.mar - 3.apr**

- Wrote some code and make some changes to some document like vison and protject manual. Joined meetings and discussed what the plan about the future of the project

## Week 7:
**4.apr - 10.apr**

- Codeing was the main part this week to but did still write in some documents. 

## Week 8:
**11.apr - 17.apr**

- Didn't do much this week because of break. Did go true criteria for this subject to be see what i have done that matches the subject criteria for the final reppot.

## Week 9
**18.apr - 24.apr**

- Started on the main report and added one point we had removed form the vision document.

## Week 10
**25.apr - 29.apr**

- Wrote on main report, selfreport and updated gant diagram.
# Weekly Reports: André Marthinsen

## Week 1: 
**21.feb - 27.feb**
- Was assigned role of code standards responsible.
- Arranged for someone to act as customer where the person came up with a product they wanted to get made in trade for me acting as the other groups customer. To increase realism we knew little about what each of us had come up with in advance of a meeting we set up.
- Arranged a meeting with said customer where we interviewed them about the product they wanted made, features they wanted included, as well as the priority of their implementation.
- Setup of the Git repository, adding the other team members, TA and teacher to the project as well as setting up the initial Wiki tree. 

<br>

## Week 2: 
**28.feb - 6.mar**

- Created the Domain Model based on the interview with our customer.
- Created the wireframe, striving to make the menu system readable, maneuverable and forgiving to mistakes. Made the wireframe as an interactive prototype in Balsamiq where snippets of text functioned as buttons to somewhat emulate a CLI interface, emulating some of the constraints of such an interface as a consequence.
- Created a guide for usability testing used by the other group members with tasks for the user followed by questions about the program interaction.
- Performed a user test with the customer.

<br>

## Week 3:
**7.mar - 13.mar**

- Attended DevOps.
- Did some early drafting of possible classes and utility functions for string management and error handling based on wireframe and domain model.

<br>

## Week 4: 
**14.mar - 20.mar**
    

- Created a report of the user tests performed in the previous week as a proper report wasn't actually turned in with the vision document, along with some retrospective notes on what can be improved for the next iteration of the test.
- Assumed leadership role after a meeting where we went over roles as they had been initially set up, how well they had functioned and what changes could be done to improve our group dynamic.
- Drafted new, more in depth roles for the group members and presented them in a meeting, giving members more detailed responsibilities and roles, as well as the flexibility to alter them as we learn and figure out what works and what doesn't.
- Researched Agile leadership and how I can function as a motivator and enabler of teamwork. Motivation and a lack of organization/communication has been our biggest obstacle so far.
- Reworked sections in the Vision Document about Product Features and Presedence and Priority, adding a rating system to the priority of implementation of the features to enable us to easier determine what to prioritize for the MVP and final product.

<br>

## Week 5: 
**21.mar - 27.mar**
- Distributed tasks on the MVP, using some earlier code written by me in week 3 as a starting point. Encouraged members to try out pair-programming as some lacked confidence when it came to programming. Hopefully working together will act as a motivator and enable rapid sharing of information and programming knowledge.
- Created a second iteration of the Domain Model using feedback from the TA. Model wasn't effectively communicating the interaction between the objects in the domain. 
- Started work on the Tournament class, the shell for most of the programs functionality. to be continued... 
- Finished the MVP. Work was hampered by me falling sick and most group members being uncomfortable with the complexity of the program, but it came out usable enough to perform testing with.


## Week 6: 
**28.mar - 3.apr**



## Week 7: 
**4.apr - 10.apr**

- Worked on reworking the usability test after input from the teacher, separating data from results as well as changing the format somewhat so that it stands well on its own.
- Had a team meeting with the group before easter, discussing the importance of taking initiative, following up on roles and the potential outcome of not doing enough work during and after easter.

## Week 8: 
**11.apr - 17.apr**

- Began work on implementing read/write file functionality for the Hole class.
- Began work on implementing the same functionality for the Tournament class.

## Week 9: 
**18.apr - 24.apr**

- My main focus this week was to bring the code to a near completed state, implementing some necessary logic and structural changes now that file to/from has been implemented, causing some unforeseen changes in how we need the code to behave.
- Conducted a team meeting getting people up to speed on the current status of the code and the overall status of the project. Encountered some problems with getting the code to compile. Discovered code blocks had an old version of mingw causing a bug with the std::filesystem library and instructed group on how to correct the problem.

## Week 10: 
**25.apr - 29.apr**

- Tidied up some repository folders, ensuring the folder structure was more coherent. Some images and other files were lacking a copy in the repository itself, only being present directly on the wiki. Made sure these were backed up in the repo.
- Went over the Wiki itself, tidying up a few things before ensuring the System branch was up to date and fully documented, with user manual, installation guide, class diagram and project structure, while Paul handled testing.
- Put the final touches on the code, including implementing some but not all of the last features expected by the customer, as well as iterating on feedback from tests. Not all was implemented as a cutoff had to be set somewhere for us to be able to finish the actual documentation portion of the project. The focus was mainly on allowing editing of pre made tournaments, and ensuring appropriate behavior depending on file state (unplayable, played, active, so on).
- Packaged the final product as a zip file with needed directories, icon and user manual.
- Coordinated the last efforts to complete the report and other documentation, compiling it into pdf's and zip files. For turn in.

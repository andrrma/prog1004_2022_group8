## EasyPut

# Project Main Report

## Version 1.0

## Abstract
We have worked as a team to build a program that manages mini golf tournaments. The program is a stand-alone program that is programmed in C++. We have used the agile development method "Kanban" with focus on universal design and high usability through out the whole process. We have managed to develop a program that is easy to use and can handle creating and editing tournaments. With the help of diagrams we have had a clear plan on what our program should look like and how it should be built.
Through multiple testing rounds with various users we know have developed our program to be easy to understand and use. We have also done a lot of testing to find any potential weaknesses in our program. 

## Preface
The study program "Programvareutvikling" is designed to give an insight into software development and how a project is managed, documented and implemented. You also learn about universal design and privacy regulations, along with how to model information systems using the Unified Modeling Language.
The purpose of this project is to get an insight into the whole process around a software project, from start to finish, and be familiar with it. We chose this project because we thought making a tournament manager for mini-golf was a brilliant idea since it is so popular and well known. It is a game played all over the world and by all ages.
After the first meeting the client, where we got the requirements for our program, we sat down to plan how go forwards with the project. When we had a specific plan we could start the work. We started with making diagrams of what our program should look like, then ran some user tests and made some tweaks based on the feedback. Through wireframe, sequence diagram, domain model and use case diagram we had a good idea of how we wanted the program to work and look like and could start programming. First we made an MVP, ran some user tests and presented it to the client. Afterwards we tweaked the program based on feedback from both user tests and the client. We added a few more functions and tested them to make sure they worked as intended, before finalizing the product.
We have learned about different testing methods and strategies, universal design and about privacy regulations, and put it to use. Being able to use flexible system development methods and UML is another thing  we have learned a lot about. We are familiar with the way a project is managed, documented and implemented, and we know how to solve conflicts when working in a team.
We would like to thank Seyed Ali Amirshahi, our TA, Anders Christoffer Westby, and our client, Øystein Qvigstad, for all the help and support we have received through out the project period.
28/04/2022 Gjøvik
 Andre Revå Marthinsen
 Ane Malene Olaussen
 Bjørn Kristian Strand
 Jørgen Teigen
 Paul Røkke Bjørneng

## Table of Contents
[[_TOC_]]

## Assignment description
The project assignment is to develop a standalone C++ application from scratch, to handle management of multi-team tournaments. The program should be able to handle setup and administration of a tournament where you can choose format, enter teams and results, as well as show the results of a tournament. The assignment was given to us as a group project in PROG1004: Programvareutvikling.
The client is Øystein Qvigstad, reachable at oysteinq@stud.ntnu.no. He is a manager of mini-golf courses and he needed a program to manage tournaments at his establishment. Requirements for the program is found in the vision-document under chapter 5, "Product features", and it is explained in the use case diagram. 

## Chapter 1: Introduction
The purpose of this report is to give an insight into the project and the work done. The report is built up by abstract, preface and assignment description before chapters and then link to the repository, references and appendix in the end. It consists of 6 chapters; Introduction, Theory and Relevant Literature, Method, Results, and conclusion and further work. The chapters build on each other and is best read in order. Additional documentation is listed in the appendix and includes Vision Document, Project Manual and GitLab WIKI.
The original objective has been partially achieved, we had to cut short on a few planned features, including team-based play, due to time restrictions. However we have a program that is fully capable of handling tournaments with solo-players. 
We were assigned a project to develop a program to handle multi-team tournaments and document all the work done. We started 21.02.2022, had first iteration 06.03.2022, second iteration 25.03.2022 and final deadline is 29.04.2022. 

### Definition, Acronyms, and Abbreviations
**This is a list of terms, acronyms and abbreviations required to properly interpret the report**
"The project" is referring to the program "EasyPut" and the work done around it.
"Documentation" is referring to this document along with the Vision Document, Project Manual and GitLab WIKI.
"We" is referring to the team including all team members listed in the preface.
"Client" is referring to the client Øystein Qvigstad.

## Chapter 2: Theory and Relevant Literature

The project and documentation is based on the curriculum in PROG1004(Programvareutvikling) and PROG1003(Objektorientert Programmering). The program is programmed in the language C++, which is taught in PROG1005.
For the documentation and development part of the project we have based our work on the course sessions held in PROG1004. We have mostly used the teachers notes posted on blackboard. The work done around testing, user testing, and user based evaluation is based of off session 4 and session 8, and it is corresponding notes. We have used session 5 and 6, along with the teachers notes, to cover the topics UML and Universal design and be able to make our program as universal as possible.
The reader should not need any specific background knowledge to understand this report. However to be able to evaluate the work presented in a proper manner they need knowledge of software development, testing, universal design and UML.

### Application - Type

The program is a so-called "Standalone application", meaning that the application and all relevant files are stored locally on the computer, and does not need to be connected to a network in order to function properly.

### Agile Development Process

Kanban is a development methodology centered around tracking progress and minimizing bottlenecks. Agile development methods focuses on frequent, but smaller deliveries to ensure the developers get enough feedback from both user-test's and the stakeholders / clients. Agile methods like: iterative, incremental and evolutionary development-methods minimizes risk by allowing the product to be more easily adaptive to changes, and even though the feature-set or functionality of the program does not increase by a substantial amount for every iteration, it ensures that a working product is available throughout the process.

### Human Computer Interaction

Since the user has  to control everything our program does, the interaction between user and computer has been the biggest focus point in developing the coming iterations. we have focused the user tests on the programs usability and user experience, key points within these areas have been:

**Usability:**

- Learnability - How easy can a user learn the ins and outs of the program.
- Memorability - After not using the program for a while, how well easily can the user get back into the "flow" of using the program.
- Efficiency - how efficient can the user use the program, and how does the program account for "power-users".
- Errors - how many errors does the users make, the severity of the mistakes, and how easy is it to recover from the mistakes.

**User Experience:**

- User interface - How the interface is presented and how intuitive it is for a user.
- information architecture - how easy is the information to find and how is it structured.
- Don normans principles of interaction design:
- Visibility of functions, does the user get adequate feedback.
- is the program restrictive, Consistency among operations that serve similar functions
- does the presented functions accurately represent what action will be performed.

- Universal design - how can we design the program so that most people can use the program to its full potential.

### Unified Modeling Language (UML)

- Use-case - written description of how the different tasks in our program will be performed from a users point of view.
- Domain model - how the objects in our domain is related to each-other.
- Sequence diagram - how the objects interact arranged on a timeline.
- Class Diagram - the structure of how our classes are structured in the domain.

### Testing

- Risk poker: evaluate what aspect of the system is the most at risk for errors, and prioritize that for testing.

- Continuous integration: writing code and testing it goes in a continuos loop to validate that the implemented functions works as intended.

- Smoke testing / sanity testing: informal test where the developers of the code does a quick test to make sure the core functionalities works as intended, before passing it on to more advanced tests.

- System testing: testing a fully integrated system to ensure it works as intended before performing user-tests or delivering the final product.

- Regression testing: testing the system before and after applying changes to ensure that the new changes does not "break" the program.

- Destructive testing: intentionally trying to make the program fail.

### Documenting

- Vision - what is the goal of the project, what is the expectation of the stakeholders?

- WIKI - documenting in forms of visuals and models as opposed to just text.

## Chapter 3: Method

_This section will discuss the methods used in the program and how the theories listed in the previous section._

### Development process and iterations:

in this project we wanted to go for an agile development method as this made the most sense in regards to the workflow we wanted to achieve. Our development process was heavily inspired by the Kanban system to quantize the tasks that was currently active, being worked on, or done for every iteration. As the next iteration begins, all the tasks should be in the "Completed" tab, these tasks were moved into the "Closed" tab of our Issue Board, and new issues would be added to the "To Do" tab. Since we wanted to focus on the Kanban method, we adapted that method to our issue-board on Gitlab to be functioning as our Kanban board. We did this to have control over what tasks was relevant to the current iteration, what tasks are currently assigned to a developer, and how far we've come in the iteration.

<br>

### Human Computer Interaction

When creating the program, we would reference Don Normans principles of interaction design (lecture notes) to make the program as intuitive as possible.
visibility, feedback and consistency was the biggest factors in our program given that it has to be a command-line-interface program. We focused little on mapping and affordance since it is not that relevant for a CLI program.

Given that we developed the program from start to end for every iteration, we have full knowledge of how the system works, and the functionality of it, therefore, we decided to focus our user-tests on Usability and User Experience.
We needed outside opinions on the learnability, memorability and universal design in order to ensure that the program would be easy to use for users with no prior experience or knowledge of our program, we also decided to have a variation in the demographic we chose to do the user tests on, to make sure it is accessible for most people.

We wanted to make the program as intuitive as possible to minimize the amount of errors that would occur due to confusion, by organizing the information architecture of the program to only be displaying the relevant information on screen, in addition to giving the user feedback on what changes were made when they would perform an action.
Should the user perform a mis-input, the system allows for correction of the inputted data, to make that feature more intuitive, we made it so that the function would change its name, based on the status of that function, for example, when entering the name of a player, "Enter Name" would change to "Change Name".

giving that the program can only get its input from the keyboard, it is generally friendly for users wanting a greater level of efficiency from the program since they often use keyboard shortcuts / macros to maneuver the menus more effectively. Even tho it is "power-user friendly" it is not something we have focused a lot on in the development process.

<br>

### Unified Modeling Language (UML)

in the early planning phases of the program, the UML diagrams proved a useful tool for the development of the prototypes, since the diagrams describe the program from many different perspectives, it is an easy way to get an overview of the program from multiple points of view.
The use-case diagram was useful to understand how the program would interact with the user to perform different tasks from the users point of view, because of that, we would reference the use-case diagram when creating the menu-system of the program.

The domain model shows us how what the different objects in our domain are, and how they relate to each other, the model represents real world objects that has to be modeled in the program, in our case for instance: the ruleset would be an object "governed" by the tournament, and contains different parameters that would make up the "ruleset object", this was useful to keep track over what data was needed for every object in our program, as well as how they would interact with the other objects.

The sequence diagrams were useful to understand how the functions in the could would be written in regards to the timeline, what operations has to happen after a given input, and how should the program respond to different inputs for the same question, for example, the user was supposed to be able to choose between "Teams" or "No teams".

The Class diagram gives us an overview of all the Classes in our code and what functions are associated with those classes, this made it easier for us to develop the code, as we could reference the class diagram to ensure all the functions and attributes for each class was integrated.

<br>

### Testing

As briefly mentioned on the sub-section about HCI, we focused most of the user tests on usability and user experience, this was because we would use continuous integration and smoke / sanity testing to make sure the functions we implemented worked before adding it to the source-code.

Smoke and Sanity testing was performed by the person writing the code to make sure the implemented pieces of code did not break the system and allowed us to make immediate changes should it cause problems for the program. before we would clear the program for formal user-tests we would also do Destructive testing to make sure the program was robust and could handle being fed the wrong type of input, in addition, we would do a final system test to confirm the program was fit for user-tests by making sure the core functionalities was working as intended and it had no major bugs that would throw the user of their task at hand.

The user-tests tasks would focus on the users ability to learn the program, remember how to perform a task after not using a set of functions for a given time, and their ability to correct any false-inputs.
The Review-questions part of the user test focused on getting their feedback on how the program "felt" to use, was it intuitive? was the information represented in a structured and easy to find manner? and other questions regarding the general User Experience. we also quickly went over the fact that we wanted to have a wide demographic represented in our user test to make sure our design choices was accessible to most people.

After completing user tests for the different iterations, we would write a summary from the user test report where we would take any concerns or notes for improvement, and condense dem into multiple problem-statements that we would suggest fixes for in team meetings. We used risk-poker in a slightly different way as to how its intended to be used, we would use risk-poker to asses what problem-statements should be of highest priority for fixes until the next user test. we also would use it in its intended use to asses what features would be most detrimental to the system, should they not function properly in the final product. This meant that some of the features that was labeled as "could have" or "should have" did not make it into the final product as we would focus on having a stable and robust system with fewer features than originally intended.

<br>

### Documentation

Our method for documentation was making sure that any work we did had to be found in one way or another on the wiki or in our repository. we had some problems with the Gantt chart, along with the time-sheets, given that these are spreadsheets, and git cannot manage version control or merging two versions of a spreadsheet into one, to solve this problem we put the spreadsheets in a personal teams-chanel so that all of us had access to the latest version, at the same time. This introduced the problem of the lack of version control, and not being able to see what or when something has been committed, but we concluded that the best way to handle it was still to have it in the teams channel.

### Work and Role Distribution
We started of the project with a role distribution that ended up working poorly due to a lack of clear tasks for each member, we ended up revising the roles the week before the second iteration was turned it (18.Mar).
The second revision focused more on clear tasks and areas of responsibility for each member.

**The second revision of the role-distribution:**

We settled on five different roles for this project:

- Project Leader: André Marthinsen
- Meeting Responsible: Paul Bjørneng
- Archiving / Documentation 1: Ane Malene Olaussen
- Archiving / Documentation 2: Jørgen Teigen
- Modeling and User-testing: BK

It was intended that the member assigned to a specific role, would be responsible for distributing the workload to various members of the group along with the specific tasks every member would have related to the role.

An example of this would be that the "Archiving / Documentation" role would have the responsibility for distributing the workload in the vision document to the rest of the group, and at the same time have the entire responsibility for turning in work on blackboard.

<br>

**_A full list of the roles and who is responsible for the different tasks,se the revised roles document that can be found under Documentation->Organization->Role_revision_draft.md in the Git Repository_**

<br>

### Tasks for everyone:

- Creating and managing git issues and tracking
- Turning in their own weekly report
- Registering hours
- Pushing work to git
- Updating wiki with documentation
- Don’t assume others are doing something
- Quality check of own work

<br>

## Chapter 4: Results
As we approach the deadline for delivering our final product as a group, we naturally start reflecting on; to which degree the actual work process has gone according to the initial plan. This, for instance, includes the similarities of the end result of this project, and our preliminary project goals and plans. This chapter will address both the project results and the administrative results, split in two different subsections. For more in depth discussions and analytic evaluations, see Chapter 5.

### Project Results

The vision document contains our earliest and initial desires in regards to the goals for this project and/or program, both short term and long term. In hindsight, it is interesting to see how our goals have remained quite universal, and still very much applicable to this project. There are however some inconsistencies concerning the status of these goals, at the present time.

Considering the different subsections of goals, listed in the vision document, it becomes clear that some goals have been weighted slightly differently. The impact goals especially, are easy to state on paper, and should satisfy the demands of most modern businesses today, given the rapid growth and usage of technology. In practice however, there is no easy way of saying if the impact goals of this project could be realized, without actually releasing "EasyPut", for companies to use. Some examples of goals where this is the case are: "Better customer service" and "Reduced long term costs". The status of the following goals however; "Streamline the process of tournament organizing" and "Less need for manual labour", are on target and remains pertinent.

The main result goal was to "create an application meant for a mini-golf tournament organizer, as an easier solution to the aspect of organizing tournaments". This goal is still very much relevant, and, for measure, we are ahead on this goal. Similarity, we are on target in regards to the process goals listed. For example, the group have certainly become acquainted, as well as obtaining a more thorough understanding of the contents of the course. The goal of "distributing roles and utilize them in the best possible way", we were quite behind on formerly, but at the present time this goal is on target. With regards to the actual product we are ahead of schedule, and the program should be ready to operate, though there are some features that are omitted from this version.

In the table below, there is an overview of which features that is included in "EasyPut" at the time of delivery. The features that are omitted from the system are generally ones that have no direct impact on the functionality, but are easy to add in the future if it should be desired.


| Nr. | Description | Comment |
|---|---|---|
| 1 | CLI Interface | Implemented feature. |
| 2 | Save/Load to/from File | Implemented feature. |
| 3 | Set-up of Tournament | Implemented feature. |
| 3.1| Registration of Teams | Not implemented. |
| 3.2 | Seeding of Players | Not implemented. | 
| 4 | Setting Tournament as Active | Implemented feature. |
| 5 | Management of an Active Tournament | Implemented feature. |
| 6 | Loading/Viewing Finished Tournament From File  | Implemented feature. |
| 7 | "Stroke Play" Scoring Format | Implemented feature. |
| 8 | "Stableford" Scoring Format | Not implemented. |
| 9 | "Match Play" Scoring Format | Not implemented. |

---

**Results from testing**

For the different types of user tests we have performed, we have asked multiple participants a series of questions as to how they experience the program, and what, if any, could be improved upon or added. Firstly the wireframe test. Here there were a lot of yes or no answers that were given, which of course is not optimal when developing a program. However we did manage to find some frequent answers. This includes: confusing menus, present a clearer option to go back (esc), and remove occurrence of repeating options in the menus. The features we decided to add from this were:
- Change layout for the main menu ("manage tracks instead of administrate tracks).
- Change from "administrate tournaments" to "Active tournament" and put finalized tournaments another place.
- On update score, add for individual player / for current hole.
- Remove display results from main menu and have put it in active tournament instead.
- Implement ESC as a way to cancel input.
- Change sequential data input to a menu style.

Next, the MVP user test. The purpose of this test was to assess functionality and the intuitivity of the program. Similarity to the Wireframe test, some of the feedback we got, we reduced to: setting/changing player name, registering strokes/holes, confusing menu system, and creating a new tournament. From this, we decided to implement:

- Add communicative text to better explain the situation (in relation to registering strokes and setting player name)
- Add "breadcrumbs" along the top of the screen, so that the user knows where they are in the menu system.
- Add a point to the User-manual that explains better how the CLI works.

And lastly, we performed an ultimate test of the actual coding of the program. These tests are separated and grouped into four files. As these were executed quite late in the works, there were not that much fallacious code to correct. A problem was a faulty loop in regards to the "finalize tournament" function, where an error message was displayed, even though the surplus of the function performed correctly. The last problem was in regards to the saving of a newly created tournament, where if there already was a tournament opened, the existing file was overwritten. These problems were fixed forthwith.

### Administrative Results
For any project to be successful, it is especially important to make a progress plan. This is to always have something to measure up the work you have done, and how you stand to finish within a specified time period. As it often happens, the definite progress plan is not always followed to the letter, which applies in our case aswell.

When initially planning for this project, it was incredibly important for us to be as effective as possible. We were all familiar with how previous, and disparate, projects have performed when effectiveness has not been in focus, and so during this planning period, we kept that in mind. In the Gantt diagram, our progress plan is portrayed in an easy to read format, and is separated into four phases, essentially into the four iterations including the final submission. In the early stages of the project (phase 1), we immediately started the preparation for the first iteration, along with distinguishing the specific tasks, and sub-tasks, that needed to be completed within May 6th. In retrospect, these tasks were almost perfectly executed, showing that the planning for it ultimately was satisfactory. With regards to our specific goals, mentioned above, we were ahead on all accounts.

Dissimilarly, for phase 2 it was clear that the initial role distribution was not working out according to plan, and that slightly put a stopper to our progress. The progress suffered essentially as there was confusion as to what our individual tasks were. This ultimately was a factor in us not reaching our goal of being close to finished with the documentation before easter, which originally was the plan. The coding was however coming along nicely, though that also had to be de-prioritized to catch up on the documentation of the vision document, summons, weekly rapports, and so forth.

Phase 3 in the progress plan included putting the finishing touches to the whole project. As aforementioned, the intent was for us to have the basic skeleton of all documents in place, by the start of phase 3, so that we could focus on the finishing touches, leading up to the final delivery. Instead we have had to really intensify the labour towards the deadline, which of course is not necessarily optimal. We are however on track, with reference to our main result goal, to deliver an acceptable end product.

**Hourly accounts**

Concerning our hourly accounts, there are some complications. Firstly, our discipline while recording hours have not been perfect. Some hours have indeed been lost, which makes it difficult to associate them with the actual work that has been done. Generally though, you can easily see how our working hours have been distributed throughout the weeks. In the first week, the hours were up to standard with what was to be expected, and we were very much on track to reach 110 hours each. This matches with our initial effectiveness and work planning. The same also goes for week 2 through 5, though with more actual documentation hours, working towards finishing the first and second iteration. For the following weeks however, we find the greatest lack of hours registered. This mainly includes the weeks between the second iteration and the final delivery date. The hours we do have registered mainly concern working on the "EasyPut" program, but also some documentation.

For more in depth explaining of hourly accounts and week-for-week status rapports, see the appendix. 

## Chapter 5: Discussion

As mentioned in the previous chapter, the end results of projects, including this one, often strays significantly from the original plans. You do not often get by, maneuvering extensive projects and developments, without encountering certain issues which could not easily have been predicted. This chapter contains several discussion points and analytic evaluations of these kinds of issues, that has been encountered during working on this project.

The end product, and the main result goal of this project, was of course the program "EasyPut". More accurately the main goal was to create an application meant for a mini-golf tournament organizer, as an easier solution to the aspect of organizing tournaments. Keeping this in mind, the development part of this project have certainly gone according to plan. We have a fully, ready to use program to present to our client, and that of course is a reason for satisfaction. Most features required from the client, have been implemented and the foundation is set for further, future expansions and updates. Some of these features include: a CLI interface, the set-up of - and also being able to log - tournaments, the "Stroke Play" scoring format, and the management of active tournaments such as updating strokes and scores. These are also some of the most crucial features needed. 

Some features however, had to be excluded from this version. The main reason being the timespan given to develop the program. Some of these features include: being able to register teams, the seeding of players, and two different scoring formats; "Stableford" and "Match play". These are features that are dispensable, considering how they are not essential for the overall functionality, and could easily be added in the future. Another limitation to the system though, is that if you were to register a wrong score, for a particular hole, there is not a way to change this without manually manipulating the save-file. This also goes for deleting a completed tournament. Lastly, there is presently only one way to update the number of strokes, namely in order of player number and holes.

The process for developing the program seemed to work quite well, especially in the beginning of the project, and for some time it looked to be possible to include all features given by the client. A reason for not including these at the present time, was all the documentation that was expected to be delivered as well. We realized at some point, that the coding had to be deprioritized. Our role distribution problem, as aforementioned, was a main factor in regards to this. Our effectiveness and productivity was significantly staggered, and while we got a good head start in phase 1 of our progress plan, in phase 2 we had to do some reorganizing. While the situation in itself was not optimal, the fact that we relatively quickly recognized the problem was exemplary. Besides, as for the technology and collaborative tools used, mainly "GitLab", the group was easily able to collaborate with the coding. 

There were several problems occurring when working on the documentation, including the collaborative features of GitLab, which, unlike when coding, we found to be more bothersome than helpful. At all times, we had to be careful not to cause merge conflicts, by pushing updates on a document while others were working on it. We initially started the writing process using other tools, such as Microsoft Teams, and unlike GitLab it was much easier to get constant updates on what the other group members were doing. We did gradually make it work though, by separating what each members tasks were, and also by using the issue boards to ensure we each knew our individual responsibilities.  

When it comes to the actual achievements concerning the documentation, and the different phases of the Gantt diagram, you can easily see the results of our struggles. As mentioned in chapter 4, our Gantt diagram is sequenced into four main phases, each essentially describing the three iterations, as well as a coding phase.  While phase 1 in the progress plan went according to our ambition, the coming weeks were characterized by some struggles. Our effectiveness suffered at the hands of, among other things, quite poorly distributed roles, and confusion regarding our individual responsibilities. Luckily we were ahead of schedule in regards to the coding part of this project, so we could afford for it to be deprioritized, while we sorted out how to better the documenting situation. The documentation in question were mainly concerning the Vision document and the project manual. For phase 3 (and 4) the labour was extensively intensified, mostly because of the problems from phase 2. The program, «EasyPut», was coming along nicely, but again, we were behind on most of the documentation. This included the vision document, the main rapport, and the weekly rapports. This certainly, was not according to plan, and had to be effectively dealt with for us to deliver an acceptable end product, with all documentation required. 

**Reflection on the team process**

Even though the end product turned out fine, the team process have been quite arguable at times. We associate both good and bad to our experiences regarding collaboration, when reflecting back on this project. As it all started out with pre made groups, we essentially did not have any prior familiarity with the other group members. So naturally, the first order of business should have been to get to know each other better. This could certainly have been more in focus, as we did not spend much time initially with any team-building. Time would show, that our means of collaboration suffered from this. We have broken down some points here, regarding what could have been done differently with the team process. The points are as follows: communication, division and implementation of labour, involvement, feedback, and presence. 

Firstly, communication. Quite an extensive problem, if not actively focusing on improving it. The positive aspects regarding this we have found to be the meetings, or rather the initial willingness for communication by showing up to the meetings. In the end though, it became clear that we had some problems with a lack of engagement. Also, at times there were too little initiative when trying to establish two-way communication. In retrospect, there were some possible actions we could take to counteract this, such as: making an effort to contribute to the conversation by giving each other instant feedback so as to create an environment of inspiration and creativity. We could also involve each other more in the thought process of different tasks, along with allowing, to an increasing extent, different opinions on important matters. 

In regards to the division of labour point, there were also some potential for improvement. In the first place, we could all improve on familiarizing ourselves with the appropriate subject matter. This way we could have gotten a better grasp of the expected effort needed to contribute adequately. Futhermore, it would most certainly allow for more optimal collaboration and cooperation if we all showed up to our meetings having read up, and prepared more beforehand. In addition, the regularly scheduled status rapports we performed would also have increased in quality from this. 

Considering the risk analysis in the vision document, there are indeed some risks we have encountered during these ten weeks. For instance the risk of lacking motivation. We initially put this down as a medium risk overall, with a score of 4. At certain times this indeed applied. The solution for preventing this risk was to do more social stuff, and focus on working together, with lots of positive feedback. As mentioned above, we very much lacked focus on these possible solutions, and in retrospect, this contributed to the communication problems we experienced throughout the project. Other risks such as "wrong focus", "misunderstanding of the task" and "miscommunication" also applies here, and were factors to multiple problems in the team process. 

If we were to give some recommendations for others who may read this rapport, who work with similar issues, they would definitely include to focus on initial team-building. This shows to be very important for any team based project, as it should help with the overall communication and collaboration. Furthermore, it would be important to plan well. Plan ahead as much, and as extensively as possible, to ensure that every group member knows what is to be expected from them when working on different parts of the project. 

## Chapter 6: Conclusion and Further Work

In this document we have presented for you our product, an application for mini-golf tournament organizing, namely “EasyPut”. We have presented the relevant theory and literature we have used for the duration of this project and chosen processes that fit our needs. Similarly, we have presented our earliest goals and requirements regarding this project, both when it comes to system requirements and features at the time of delivery, but also the status of our process-, impact-, and result goals. We have seen, while working over this 10-week period, how different problems have occurred and how we have attempted to solve them without the time usage suffering too much. 

Even though we have had problems throughout, our main goal was indeed reached, and a fully functional program has been made. We have analyzed what could have gone differently, and we have all certainly learned a lot. This will be good to keep in mind for future, similar projects. 


## Repository

https://gitlab.stud.idi.ntnu.no/andrrma/prog1004_2022_group8

## References

Amirshahi. S. A. 2022, Software Engineering (IDATG1002 and PROG1004)\
(lecture notes)

## Appendix